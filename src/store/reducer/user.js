import * as actionTypes from "../actions/actionTypes";
import util from "../../components/util/util";

const InitialState = {
	isLogged: false,
	location: {
		country: "Россия",
		city: "Сочи"
	},

	authState: {
		loading: false,
		error: false
	},
	regState: {
		loading: false,
		error: false
	},
	profile: {},
	cart: {
		partners: [],
		total: 0,
		positions: 0
	},
	customerData: {
		filter: {
			dateIn: util.moment().add(1, "day"),
			dateOut: util.moment().add(7, "day"),
			time: "10:00",
			adults: 2,
			children: 0,
			subCategory: "Все"
		},
		searchTerm: "",
		category: null,
		sort: "ASC"
	},
	favourites: []
};

/**
 *
 * Location
 *
 */
const setLocation = (state, action) => {
	return {
		...state,
		location: {
			...action.location
		}
	};
};

const getCart = (state, action) => {
	return {
		...state,
		cart: action.cart
	};
};

const setUser = (state, action) => {
	return {
		...state,
		isLogged: true,
		profile: action.user
	};
};

const removeFavourite = (state, action) => {
	const favourites = [...state.favourites].filter(
		e => e.id !== action.item.id
	);
	return {
		...state,
		favourites
	};
};

const addFavourite = (state, action) => {
	const favourites = [...state.favourites, action.item];
	favourites.push(action.item);
	return {
		...state,
		favourites
	};
};

/**
 *
 * Authentication
 *
 */

const authPending = (state, action) => {
	return {
		...state,
		authState: {
			loading: true,
			error: false
		}
	};
};

const authReset = (state, action) => {
	return {
		...state,
		authState: {
			loading: false,
			error: false
		}
	};
};

const authSuccess = (state, action) => {
	return {
		...state,
		isLogged: true,
		authState: {
			loading: false,
			error: false
		},
		profile: {
			...action.user
		}
	};
};

const authNotPerformed = state => {
	return {
		...state,
		isLogged: false,
		authState: {
			loading: false,
			error: false
		},
		profile: {}
	};
};

const authFailed = (state, action) => {
	return {
		...state,
		authState: {
			loading: false,
			error: action.error
		}
	};
};
/**
 *
 * Registration
 *
 */

const regPending = (state, action) => {
	return {
		...state,
		regState: {
			loading: true,
			error: false
		}
	};
};

const regReset = (state, action) => {
	return {
		...state,
		regState: {
			loading: false,
			error: false
		}
	};
};

const regSuccess = (state, action) => {
	return {
		...state,
		isLogged: true,
		regState: {
			loading: false,
			error: false
		},
		profile: {
			...action.user
		}
	};
};

const regFailed = (state, action) => {
	return {
		...state,
		regState: {
			loading: false,
			error: action.error
		}
	};
};

/**
 *
 * Customer Data
 *
 */

const setFilter = (state, action) => {
	return {
		...state,
		customerData: {
			...state.customerData,
			filter: {
				...state.customerData.filter,
				...action.filter
			}
		}
	};
};

const setCategory = (state, action) => {
	return {
		...state,
		customerData: {
			...state.customerData,
			category: action.category ? action.category : null
		}
	};
};

const setTerm = (state, action) => {
	return {
		...state,
		customerData: {
			...state.customerData,
			searchTerm: action.term ? action.term : ""
		}
	};
};

const setSort = (state, action) => {
	return {
		...state,
		customerData: {
			...state.customerData,
			sort: action.sort
		}
	};
};

/**
 *
 * Reducer
 *
 */

const reducer = (state = InitialState, action) => {
	switch (action.type) {
		case actionTypes.GET_CART:
			return getCart(state, action);

		case actionTypes.AUTH_PENDING:
			return authPending(state, action);
		case actionTypes.AUTH_SUCCESS:
			return authSuccess(state, action);
		case actionTypes.AUTH_RESET:
			return authReset(state, action);
		case actionTypes.AUTH_FAILED:
			return authFailed(state, action);
		case actionTypes.AUTH_NOT_PERFORMED:
			return authNotPerformed(state, action);

		case actionTypes.REG_PENDING:
			return regPending(state, action);
		case actionTypes.REG_SUCCESS:
			return regSuccess(state, action);
		case actionTypes.REG_RESET:
			return regReset(state, action);
		case actionTypes.REG_FAILED:
			return regFailed(state, action);

		case actionTypes.SET_USER:
			return setUser(state, action);
		case actionTypes.ADD_FAVOURITE:
			return addFavourite(state, action);
		case actionTypes.REMOVE_FAVOURITE:
			return removeFavourite(state, action);

		case actionTypes.SET_FITLER:
			return setFilter(state, action);
		case actionTypes.SET_CATEGORY:
			return setCategory(state, action);
		case actionTypes.SET_TERM:
			return setTerm(state, action);
		case actionTypes.SET_SORT:
			return setSort(state, action);

		case actionTypes.SET_LOCATION:
			return setLocation(state, action);

		default:
			return state;
	}
};

export default reducer;
