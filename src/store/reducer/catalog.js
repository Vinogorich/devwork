import * as actionTypes from '../actions/actionTypes';


const InitialState = {
    categories:[]
}

const setCategories = (state,action) => {
    const categories = action.data;
    return {
        ...state,
        categories
    }
}



const reducer = ( state = InitialState, action ) => {
    
    switch ( action.type ) {
        case actionTypes.SET_CATEGORIES: return setCategories(state,action);

        default: return state;
    }
};

export default reducer;