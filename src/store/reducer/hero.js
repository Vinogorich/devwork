/**
 * 
 * Deprecated
 * 
 */
const InitialState = {
    categories:[],
    catalog:{},
    selectedCategory:null,
    locations:[],
    selectedLocation:null
}

const changeCategoryAction = (state,newCategory) => {
    return {
        ...state,
        selectedCategory : newCategory
    };
}

const updateCategoriesAction = (state,categoriesList) => {
    
    return {
        ...state,
        categories : categoriesList
    };
}
const updateCatalogAction = (state,catalogItem) => {
    
    return {
        ...state,
        catalog : {
            ...state.catalog,
            ...catalogItem
        }
    };
}

const reducer = ( state = InitialState, newState ) => {
    
    switch ( newState.type ) {
        case 'changeCategory': return changeCategoryAction(state,newState.value);
        case 'updateCategories': return updateCategoriesAction(state,newState.value);
        case 'updateCatalog': return updateCatalogAction(state,newState.value);
        default: return state;
    }
};

export default reducer;