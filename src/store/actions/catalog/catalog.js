import * as actionTypes from "../actionTypes";
import api from "../../../components/API/new/API";
import util from "../../../components/util/util";

export const getCategories = () => {
    return dispatch => {
        api.catalog
            .GetSubCategory()
            .then(response => {
                if (!response.data.error) {
                    const data = response.data;
                    dispatch({
                        type: actionTypes.SET_CATEGORIES,
                        data
                    });
                } else {
                    const message = response.data.msg
                        ? response.data.msg
                        : "Невозможно получить категории каталога";
                    dispatch({
                        type: actionTypes.REQ_FAILED,
                        error: message
                    });
                }
            })
            .catch(e => {
                const message = "Произошла ошибка сервера";
                dispatch({
                    type: actionTypes.REQ_FAILED,
                    error: message
                });
            });
    };
};
