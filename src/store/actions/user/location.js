import * as actionTypes from '../actionTypes';

export const setLocation = (location) => {
    return dispatch => {
        dispatch({
           type:actionTypes.SET_LOCATION,
           location
        })
    }
}