import * as actionTypes from '../actionTypes';
import api from '../../../components/API/new/API';
import util from '../../../components/util/util';

export const addToCart = (item) => {
    return dispatch => {
        api.commerce.AddToCart(item).then( response => {
            dispatch({
               type:actionTypes.GET_CART,
               cart:response.data
            });
        }) 
    }
}

export const removeFromCart = (items) => {
    return dispatch => {
        api.commerce.RemoveFromCart(items).then( response => {
            dispatch({
               type:actionTypes.GET_CART,
               cart:response.data
            });
        }) 
    }
}


export const getCart = () => {
    return dispatch => {
        api.commerce.GetCart().then( response => {
            dispatch({
               type:actionTypes.GET_CART,
               cart:response.data
            });
        }) 
    }
}
