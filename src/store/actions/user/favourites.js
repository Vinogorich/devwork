import * as actionTypes from '../actionTypes';
import api from '../../../components/API/new/API';

/**
 * Cart
 */

/* export const addTocart = () => {
    return dispatch => {
        api.commerce.addToCart
    }
}

export const getCart = () => {
    return dispatch => {
        api.commerce.GetCart().then( response => {
            dispatch({
               type:actionTypes.GET_CART,
               cart:response.data
            });

        }) 
    }
}
 */

/** 
 * 
 * Authentication
 * 
*/

/* export const login = (data) => {
    return dispatch => {   
        dispatch({
            type:actionTypes.AUTH_PENDING,
        })
        api.user.Login({...data}).then(response => {
             if (!response.data.error) {
                dispatch({
                    type:actionTypes.AUTH_SUCCESS,
                    user:response.data.user ? response.data.user : response.data
                })
            } else {
                const message = response.data.msg ? response.data.msg : 'Авторизация не пройдена';
                dispatch({
                    type:actionTypes.AUTH_FAILED,
                    error:message
                })
            }
        })
        .catch( e => {
            const message = 'Произошла ошибка сервера';
            dispatch({
                type:actionTypes.AUTH_FAILED,
                error:message
            })
        })

    }
}

export const authReset = () => {
    return dispatch => {
        dispatch({
           type:actionTypes.AUTH_RESET
        })
    }
}

export const logout = () => {
    return dispatch => {   
        dispatch({
            type:actionTypes.AUTH_PENDING,
        })
        api.user.Logout().then(response => {
             if (!response.data.error) {
                dispatch({
                    type:actionTypes.AUTH_NOT_PERFORMED,
                })
            } else {
                const message = response.data.msg ? response.data.msg : 'Завершение сессии не удалось';
                dispatch({
                    type:actionTypes.AUTH_FAILED,
                    error:message
                })
            }
        })
        .catch( e => {
            const message = 'Произошла ошибка сервера';
            dispatch({
                type:actionTypes.AUTH_FAILED,
                error:message
            })
        })

    }
} */

/** 
 * 
 * Registration
 * 
*/

/* export const regReset = () => {
    return dispatch => {
        dispatch({
           type:actionTypes.REG_RESET
        })
    }
}

export const register = (formData) => {
    return dispatch => {
        dispatch({
            type:actionTypes.REG_PENDING,
        })
        api.user.Register({...formData})
            .then(response => {
                if (!response.data.error) {
                    dispatch({
                        type:actionTypes.REG_SUCCESS,
                        user:response.data.user ? response.data.user : response.data
                    })                
                } else {
                    const message = response.data.msg ? response.data.msg : 'Регистрация не пройдена';
                    dispatch({
                        type:actionTypes.REG_FAILED,
                        error:message
                    })
                }
            })
            .catch( e => {
                    const message = 'Произошла ошибка сервера';
                    dispatch({
                        type:actionTypes.REG_FAILED,
                        error:message
                    })
            })
    }
}

 */
/**
 * 
 * Faourites
 * 
 */
export const addFavourite = (itemData) => {
    
    return dispatch => {
        dispatch({
            type:actionTypes.ADD_FAVOURITE,
            item: itemData
        })                
        
    }
}

export const removeFavourite = (itemData) => {
    return dispatch => {
        dispatch({
            type:actionTypes.REMOVE_FAVOURITE,
            item: itemData
        })                
        
    }
}


 