//Cart functions
export { addToCart, getCart, removeFromCart } from "./user/cart";

//Registration
export { register, regReset } from "./user/register";

//Authentication
export { login, authReset, logout } from "./user/auth";

//Favourites
export { addFavourite, removeFavourite } from "./user/favourites";

// Customer data
export { setFilter, setCategory, setTerm, setSort } from "./user/customer";

//Catalog Store functions
export { getCategories } from "./catalog/catalog";

//Location set
export { setLocation } from "./user/location";
