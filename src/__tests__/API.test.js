import api from '../components/API/new/API';
describe(' API ',()=>{
    it('should work', () => {
        expect(Object.keys(api).length).toBeGreaterThan(0);
    });
    it('should return  async BestProducts', async ()=>{
        expect.assertions(1);
        const data =  await api.search.BestProducts();
        expect(parseInt(data.status)).toBe(200);
    })
});