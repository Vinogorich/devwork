import { createStore } from 'redux';
import { Provider } from 'react-redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import React from 'react';

const Store = (props) => {
    const reducer = require (`./../../store/${props.reducer}`).default;
    let store = createStore(reducer,composeWithDevTools() );
    return (
        <Provider store={store}>
            {props.children}        
        </Provider>   
    )
}

export default Store;