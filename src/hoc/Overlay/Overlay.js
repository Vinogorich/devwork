import React, { Component } from "react";
import css from "./Overlay.module.scss";
import Icon from "@material-ui/core/Icon";

const withOverlay = WrappedComponent => {
  return class extends Component {
    render = () => {
      return (
        <div onClick={this.props.hideHandler} className={css["container"]}>
          <div>
            <WrappedComponent {...this.props} />
            <Icon
              fontSize="large"
              className={css["close"]}
              onClick={this.props.hideHandler}
            >
              close
            </Icon>
          </div>
        </div>
      );
    };
  };
};

export default withOverlay;
