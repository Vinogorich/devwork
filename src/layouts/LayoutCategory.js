import React, { Component } from "react";
import Hero from "./../components/Hero/Hero";
import CategorySlider from "./../components/CategorySlider/CategorySlider";
import { connect } from "react-redux";
import Banner from "../components/Banner/Banner";
import Wrapper from "../hoc/Wrapper/Wrapper";
import util from "../components/util/util";
import * as actions from "../store/actions/index";

class LayoutCategory extends Component {
    state = {
        category: null,
        offers: []
    };

    componentDidMount = () => {
        if (this.props.catalog.length > 0) {
            this.syncUrlCategory();
        }
    };

    componentDidUpdate = (prevProps, prevState) => {
        if (this.props.catalog.length !== prevProps.catalog.length) {
            util.log("catalog recieved");
            this.syncUrlCategory();
        }
    };

    syncUrlCategory() {
        const { category } = this.props.match.params;
        util.log(category);
        if (category && this.props.catalog.length > 0) {
            const _currentCategory = this.props.catalog.filter(
                item => item.code === category
            );
            if (_currentCategory.length > 0) {
                this.props.setCategory({ ..._currentCategory[0] });
            }
        }
    }

    render = () => {
        const sliders = this.props.category
            ? this.props.category.subcat.map((item, index) => {
                  return (
                      <CategorySlider
                          key={item.id}
                          category={this.props.category}
                          subCategory={item}
                      />
                  );
              })
            : null;

        return (
            <Wrapper>
                <Hero />
                {sliders}
                <Banner />
            </Wrapper>
        );
    };
}

const mapStateToProps = state => ({
    catalog: state.catalog.categories,
    category: state.user.customerData.category
});
const mapDispatchToProps = dispatch => ({
    setCategory: category => dispatch(actions.setCategory(category))
});
export default connect(mapStateToProps, mapDispatchToProps)(LayoutCategory);
