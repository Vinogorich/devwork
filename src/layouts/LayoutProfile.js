import React, { Component } from 'react';
import { connect } from 'react-redux';
import AuthForm from '../components/UserForm/_AuthForm';
import UserData from '../components/UserData/UserData';
import { Panel } from '../components/Interface/Interface';
import * as actions from '../store/actions/index';

class LayoutProfile extends Component {
    componentDidMount() {
        this.props.login();
    }

    render = () => { 
        return ( 
            <section className="ut-section__container">
            <Panel>
            {
                this.props.user && this.props.user.isLogged ?
                <UserData {...this.props.user} logout={this.props.logout} />
                :
                <AuthForm />
            }
                
            </Panel>
        </section>
            
         );
    }
}
 
const mapStateToProps = state => ({
    user:state.user,
});

const mapDispatchToProps = dispatch => ({
    login: (email,password)=>dispatch(actions.login(email,password)) ,
    logout: ()=>dispatch(actions.logout()) 
})

export default connect(mapStateToProps,mapDispatchToProps)(LayoutProfile);