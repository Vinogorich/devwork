import React from 'react';
import Header from './../components/Header/Header';
import Footer from './../components/Footer/Footer';
import Wrapper from '../hoc/Wrapper/Wrapper';
import './LayoutCommon.scss';

const LayoutCommon = (props) =>  (
    <Wrapper>
        <Header />
        {props.children}
        <Footer />
    </Wrapper>
    

)

export default LayoutCommon;

