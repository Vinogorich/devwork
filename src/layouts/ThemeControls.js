import React, { Component } from 'react';
import { connect } from 'react-redux';

class ThemeControls extends Component {
    componentDidMount() {
        document.body.classList.add("theme-azul");
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.gender !== this.props.gender) {
            document.body.classList.forEach(e=> {
                if (e.substr(0,6) === 'theme-') {
                    document.body.classList.remove(e);
                }
            })
            const theme = this.props.gender === "f" ? "theme-rojo" : "theme-azul";
            document.body.classList.add(theme);
        }
    }

    render() { 
        return null;
    }
}

const mapStateToProps = state => ({
    gender:state.user.profile.gender
})
 
export default connect(mapStateToProps)(ThemeControls);