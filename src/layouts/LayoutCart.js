import React, { Component } from "react";
import css from "../components/Cart/Cart.module.scss";
import Icon from "@material-ui/core/Icon";
import Cart from "../components/Cart/Cart";
import OrderTotals from "../components/OrderTotals/OrderTotals";
import api from "../components/API/new/API";
import CustomerData from "../components/CustomerData/CustomerData";
import { connect } from "react-redux";
import * as actions from "../store/actions/index";
import util from "../components/util/util";

class LayoutCart extends Component {
    state = {
        cart: {
            partners: []
        }
    };

    componentDidMount = () => {
        this.props.getCart(cart => {});
    };

    payHandler = customerData => {
        if (this.props.user.isLogged) {
            return api.commerce.AddOrder().then(response => {
                if (response.data) {
                    const url = response.data.url;
                    window.location = url;
                } else {
                    alert("Регистрация заказа не удалась");
                }
            });
        } else {
            return api.commerce
                .QuickOrder(customerData)
                .then(response => {
                    if (response.data && !response.data.error) {
                        const url = response.data.url;
                        window.location = url;
                        //alert(url);
                    } else {
                        alert(
                            util.join(
                                "Регистрация заказа не удалась",
                                response.data.msg
                            )
                        );
                    }
                })
                .catch(e => alert(e));
        }
    };

    render = () => {
        return (
            <div className="ut-section__container">
                <h1 className="ut-section__title">
                    Моя корзина{" "}
                    <a href="#share" className="ut-section__title-link">
                        <Icon>share</Icon>Поделиться своим списком покупок
                    </a>{" "}
                </h1>
                <div className={css.layout}>
                    <div className={css.col_left}>
                        <Cart
                            removeHandler={this.props.removeFromCart}
                            partners={[...this.props.cart.partners]}
                        />
                        <CustomerData
                            user={this.props.user}
                            saveHandler={this.payHandler}
                        />
                    </div>
                    <div className={css.col_right}>
                        <OrderTotals
                            isLogged={this.props.user.isLogged}
                            payHandler={this.payHandler}
                            total={this.props.cart.total}
                            partners={[...this.props.cart.partners]}
                        />
                    </div>
                </div>
            </div>
        );
    };
}

const mapStateToProps = state => ({
    user: state.user,
    cart: state.user.cart
});

const mapDispatchToProps = dispatch => ({
    getCart: () => dispatch(actions.getCart()),
    removeFromCart: items => dispatch(actions.removeFromCart(items)),
    register: formData => dispatch(actions.register(formData))
});

export default connect(mapStateToProps, mapDispatchToProps)(LayoutCart);
