import React, { Component, Fragment } from "react";
import LayoutCommon from "./LayoutCommon";
import apiDev from "../components/API/API.dev";
import ProductData from "../components/ProductData/ProductData";
import Loading from "../hoc/Loading/Loading";
import util from "../components/util/util";
import api from '../components/API/new/API';

class LayoutProduct extends Component {
    state = {
        data: false,
        photos: [],
        products: false,
        reviews:{
            items:[],
            totals:{}
        }
    };

    componentDidMount = () => {
        this.props.loading(true);
        const { category, code } = this.props.match.params;
        const params = {
            partnerCode: code,
            categoryCode: category,
            
        };

        apiDev("catalog/GetPartnerDataByID", params).then(response => {
            this.setState(
                state => ({
                    ...state,
                    data: { ...response.data[0] }
                }),

                this.props.loading
            );
        });
        apiDev("catalog/GetPhotos", params).then(response => {
            if (
                response.data.params.photos &&
                Array.isArray(response.data.params.photos)
            ) {
                this.setState(state => ({
                    ...state,
                    photos: response.data.params.photos
                }));
            }
        });
        apiDev("catalog/GetProductsByPartnerID", params).then(response => {
            if (
                response.data &&
                Array.isArray(response.data)
            ) {
                const products = response.data;
                this.setState(state => ({
                    ...state,
                    products
                }));
            }
        });
        if (category == 'acc') {
            api.catalog.GetProductReviews().then((reviews)=>{
                this.setState(state=>({
                    ...state,
                    reviews:reviews.data
                }))
            })

        }
    };

    render = () => {
        
        return (
            <Fragment>
            {
                this.state.data ?
                <ProductData
                    data={this.state.data}
                    photos={this.state.photos}
                    params={this.state.data.params}
                    products={this.state.products}
                    reviews={this.state.reviews}
                    category = {this.props.match.params.category}
                />
                :
                <div style={{height:'80vh'}} />

            }
            </Fragment>
        );
    };
}

export default Loading(LayoutProduct);
