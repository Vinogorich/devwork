import React, { Component, Fragment } from "react";

import SearchResults from "./../components/SearchResults/SearchResults2";
import { connect } from "react-redux";
import SearchInput from "../components/SearchInput/SearchInput";
import * as actions from "../store/actions/index";
import Banner from "../components/Banner/Banner";

class LayoutSearch extends Component {
    state = { results: [] };

    componentDidMount = () => {
        const { term } = this.props.match.params;

        if (term !== this.props.term) {
            this.props.setTerm(term);
        }
        if (this.props.catalog.length > 0) {
            this.syncUrlCategory();
        }
    };

    componentDidUpdate(prevProps, prevState) {
        if (this.props.catalog.length !== prevProps.catalog.length) {
            this.syncUrlCategory();
        }
    }

    syncUrlCategory() {
        const { category } = this.props.match.params;
        if (category && this.props.catalog.length > 0) {
            const _currentCategory = this.props.catalog.filter(
                item => item.code === category
            );
            if (_currentCategory.length > 0) {
                this.props.setCategory({ ..._currentCategory[0] });
            }
        }
    }

    render = () => {
        const { term, category } = this.props.match.params;
        return (
            <Fragment>
                <SearchInput
                    term={this.props.term}
                    searchHandler={this.props.setTerm}
                />
                <div className="ut-section__container ut-section__container--no-margin">
                    <SearchResults urlCategoryCode={category} />
                    <Banner />
                </div>
            </Fragment>
        );
    };
}

const mapStateToProps = state => ({
    catalog: state.catalog.categories,
    category: state.user.customerData.category,
    term: state.user.customerData.searchTerm
});
const mapDispatchToProps = dispatch => ({
    setTerm: term => dispatch(actions.setTerm(term)),
    setCategory: category => dispatch(actions.setCategory(category))
});

export default connect(mapStateToProps, mapDispatchToProps)(LayoutSearch);
