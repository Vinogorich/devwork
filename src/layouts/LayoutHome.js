import React, { Component, Fragment } from "react";
import Hero from "./../components/Hero/Hero";
import SearchResults from "./../components/SearchResults/SearchResults";
import CategorySlider from "./../components/CategorySlider/CategorySlider";
import { connect } from "react-redux";
import apiDev from "./../components/API/API.dev";
import util from "../components/util/util";
import Banner from "../components/Banner/Banner";

class LayoutHome extends Component {
    render = () => {
        const sliders = this.props.category
            ? this.props.category.subcat.map(item => {
                  return (
                      <CategorySlider
                          key={item.id}
                          category={this.props.category}
                          subCategory={item}
                      />
                  );
              })
            : this.props.catalog.categories
                  .filter(item => item.code !== "acc")
                  .map(item => (
                      <CategorySlider key={item.id} category={item} />
                  ));
        return (
            <Fragment>
                <Hero />
                <SearchResults />
                {sliders}
                <Banner />
            </Fragment>
        );
    };
}

const mapStateToProps = state => {
    return {
        catalog: state.catalog,
        filter: state.user.customerData.filter,
        category: state.user.customerData.category
    };
};

const mapDispatchToProps = dispatch => {
    return {
        updateCategories: value =>
            dispatch({ type: "updateCategories", value }),
        updateCatalog: value => dispatch({ type: "updateCatalog", value })
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LayoutHome);
