import React, { useState, Fragment } from "react";
import CategoryChart from "./CategoryChart";
import css from "./styles/TimePicker.module.scss";

const CategoryPicker = props => {
  const [pickerVisible, setPickerVisible] = useState(true);
  let defaultValue =
    typeof props.subCategory === "string" ? "Все" : props.subCategory.name;
  let nameSubcategories = props.subCategories.map(el => {
    return el.name;
  });
  // useImperativeHandle(props.ref, () => {
  // 	setVisible(){
  // 		setPickerVisible(false);
  // 	}

  // });
  // const inputRef = useRef();
  // useImperativeHandle(props.ref, () => ({
  //   focus: () => {
  //     inputRef.current.setPickerVisible(false);
  //   }
  // }));
  // if (defaultValue == "") {
  // 	defaultValue = "Все"
  // }
  // if (typeof (defaultValue) == "object") {
  // 	defaultValue = props.subCategory.name
  // } else {
  // 	defaultValue = "Все"
  // }
  // let clickCategory = (val) => {
  // 	return defaultValue = val
  // }
  if (pickerVisible == false) {
    props.test(!pickerVisible);
  }
  let chart = (
    <CategoryChart
      arraySubCategory={nameSubcategories}
      defaultValue={defaultValue}
      subCategories={props.subCategories}
      subCategory={props.subCategory}
      // parentState={props.parentState}
      // setParentState={props.setParentState}
      // clickCategory={(val) => { clickCategory(val) }}
      test={true}
      pickHandler={sub => {
        setPickerVisible(false);
        // props.setParentState(false);
        props.pickHandler(sub);
      }}
      hideHandler={() => setPickerVisible(false)}
    />
  );

  return (
    <Fragment>
      <p
        // type="text"
        // onClick={() => props.hideHandler()}
        // value={defaultValue}
        // readOnly
        className={css["input-category"]}
      >
        {defaultValue}
      </p>
      {props.parentState && pickerVisible ? chart : null}
    </Fragment>
  );
};

export default CategoryPicker;
<CategoryPicker
// subCategory={props.filter.subCategory}
// subCategories={props.subCategories}
// parentState={btnHandler}
// setParentState={setbtnHandler}
// test={sub => {
//   setbtnHandler(sub);
// }}
// pickHandler={sub => {
//   // setbtnHandler(false);
//   props.setFilter({
//     subCategory: sub
//   });
// }}

// hideHandler={() => setbtnHandler(!btnHandler)}
// subCategories={props.subCategories}
/>;
