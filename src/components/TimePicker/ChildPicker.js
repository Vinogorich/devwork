import React, { useState, Fragment } from "react";
import TimeChart from "./TimeChart";
import css from "./styles/TimePicker.module.scss";

const ChildPicker = props => {
  let [pickerVisible, setPickerVisible] = useState(props.toggleVisible);
  let sum = () => {
    if (props.children < 1) {
      return "без детей";
    } else if (props.children == 1) {
      return props.children + " ребенок";
    } else {
      return props.children + " детей";
    }
  };
  let chart = (
    <TimeChart
      child={true}
      children={props.children}
      pickHandler={ad => {
        setPickerVisible(false);
        props.pickHandler(ad);
      }}
      hideHandler={() => setPickerVisible(!pickerVisible)}
    />
  );
  let main = props.main;
  return (
    <Fragment>
      <input
        type="text"
        onClick={() => setPickerVisible(!pickerVisible)}
        value={sum()}
        readOnly
        className={main ? css["picker-input"] : css["input-subfilter"]}
      />
      {pickerVisible ? chart : null}
    </Fragment>
  );
};

export default ChildPicker;
