import React, { useState, Fragment } from "react";
import TimeChart from "./TimeChart";
import css from "./styles/TimePicker.module.scss";

const MorePicker = props => {
  // let pickerVisible = props.toggleVisible;
  let [pickerVisible, setPickerVisible] = useState(props.toggleVisible);
  let sum = () => {
    if (props.adults < 2) {
      return " взрослый";
    } else {
      return " взрослых";
    }
  };
  let sumChild = () => {
    if (props.children < 1) {
      return "без детей";
    } else if (props.children == 1) {
      return props.children + " ребенок";
    } else {
      return props.children + " детей";
    }
  };
  let chart = (
    <TimeChart
      // adult={true}
      more={props.more}
      adults={props.adults}
      pickHandler={(ad, ch) => {
        setPickerVisible(false);
        props.pickHandler(ad, ch);
      }}
      hideHandler={() => setPickerVisible(!pickerVisible)}
      // child={true}
      children={props.children}
      // pickHandlerChild={ad => {
      //   setPickerVisible(false);
      //   props.pickHandlerChild(ad);
      // }}
    />
  );
  let main = props.main;
  let product = props.product;
  let classes = [css["moreWrapper"]];

  if (product) {
    classes.push(css.productAdults);
  }
  return (
    <Fragment>
      {/* <input
				type="text" */}
      <div className={classes.join(" ")}>
        <p
          onClick={() => setPickerVisible(!pickerVisible)}
          // value={props.adults + sum()}
          // readOnly
          className={main ? css["picker-input"] : css["input-subfilter"]}
        >
          {props.adults + sum()}
        </p>
        {/* <input
				type="text" */}
        <p
          onClick={() => setPickerVisible(!pickerVisible)}
          // value={sumChild()}
          // readOnly
          className={main ? css["picker-input"] : css["input-subfilter"]}
        >
          {sumChild()}
        </p>
      </div>
      {pickerVisible ? chart : null}
    </Fragment>
  );
};
export default MorePicker;
