import React from 'react';
import CartItem from './_CartItem';
import css from './Cart.module.scss';

const Cart = (props) => {
    const partners = props.partners.map((item,index)=> (
        <CartItem key={index} {...item} onRemove={props.removeHandler} />

    )) ;
    

    return ( 

    <div className={css['container']}>
        {partners} 
    </div>  );
}
 
export default Cart;