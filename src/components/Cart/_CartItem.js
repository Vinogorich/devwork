import React, { useState } from 'react';
import css from './Cart.module.scss';
import Icon from '@material-ui/core/Icon';
import util from '../util/util';

const CartItem = (props) => {

    const itemsSummary = props.items.map((item,index)=>(
        <li key={index}>{item.name} x {item.quantity}</li>
    ))

    const [expanded, toggleExpanded] = useState(false);
    const products = props.items.map((product,productIndex)=>{
        
        const params = product.params.map((param,paramIndex)=>{
           
            return(<li className={css['product-param']} key={paramIndex}>
                <span className={css['product-param-caption']}>{param.name}</span>
                {
                    param.edit ?
                    <input className={css['product-param-input']} placeholder={param.name} defaultValue={param.value} />
                    :
                    <span className={css['product-param-value']}>{param.value}</span>
                    
                }
            </li>);
        })
        const items = [product.id]; //TODO -- correct
        return(
            <div className={css['product']} key={productIndex}>
                <h5 className={css['product-title']}>
                    {product.name}
                </h5>
                <a className={css['product-remove']} onClick={()=>props.onRemove(items)}>
                    <Icon>delete_forever</Icon>
                </a>
                <p className={css['product-text']}>
                    { util.ellipse(product.text,160) }
                </p>
                <ul>
                    {params}
                </ul>
            </div>
    )});
    return (
            <div className={css['item']}> 
                <div className={css['item-main']}>
                    <ul className={util.join("ut-theme-color",css['item-icons'])}>
                        <li className={css['item-icon']}>
                            <Icon>done</Icon>
                        </li>
                        <li className={css['item-icon']}>
                            <Icon>clear</Icon>
                        </li>
                        <li className={css['item-icon']}>
                            <Icon>favorite_border</Icon>
                        </li>
                        <li className={css['item-icon']}>
                            <Icon>share</Icon>
                        </li>
                    </ul>
                    <img src={props.image} className={css['item-image']} />
                    <div className={css['item-data']}>
                        <h3 className={css['item-title']}>
                            {props.name}
                        </h3>
                        <ul className={css['item-summary']}>
                            {itemsSummary}
                        </ul>
                        <div className={css['item-price']}>
                            Цена {util.currencyFormat(props.total)}
                            <span className={ util.join(css['item-toggle'],"ut-theme-color")}><input type="checkbox" defaultChecked={expanded} /><label onClick={()=>toggleExpanded(!expanded)}>Подробнее</label></span>
                        </div>
                    </div>
                </div> 
                <div className={
                    util.join(
                        css['item-details'], 
                        ( expanded ? css['item-details--expanded'] : '' ) 
                    )}
                    >
                        {products}

                        <div className={ util.join( css['product'], "ut-theme-color")}>
                            <button className={css['product-button']} onClick={()=>toggleExpanded(false)}><span>Сохранить</span></button>
                        </div>
                </div>
            </div>);
}
 
export default CartItem;