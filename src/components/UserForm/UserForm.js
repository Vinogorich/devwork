import React from 'react';
import withOverlay from '../../hoc/Overlay/Overlay';
import AuthForm from './_AuthForm';
import RegisterForm from './_RegisterForm';
import Tabs from '../Interface/Tabs';

const UserForm = props => {
    return (
        <Tabs className="ut-theme-color">
            <RegisterForm title="Регистрация" />
            <AuthForm title="Авторизация"/>
        </Tabs>
        
    );
}

export default withOverlay(UserForm);