import React, { Component } from 'react';
import { Form, Input, Button } from '../Interface/Interface';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import Wrapper from '../../hoc/Wrapper/Wrapper';
import css from './UserForm.module.scss';

class AuthForm extends Component {
    state = {
        email:'',
        password:''
    }

    handleFieldChange = (e) => {
        e.preventDefault();
        const name = e.target.getAttribute("name");
        const value = e.target.value;
        this.setState((state)=>{
            const newState = {...state};
            newState[name] = value;
            return newState;
        })
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.props.auth.error) {
            const error = this.props.auth.error;
            this.props.reset();
            alert (error);
        }
        
    }

    handleAuth = () => {
        this.props.login({...this.state});
    }


    render() { 
        return ( 
            <div className="ut-form__container">
                {
                    this.props.isLogged ? 
                    <h3 className={css['title']}>Вы авторизованы</h3>
                    :
                    <Wrapper>
                        <h3 className={css['title']}>Авторизация</h3>
                        <Form>
                            <Input onChange={this.handleFieldChange} name="email" placeholder="Email" label="Введите Ваш Email"/>
                            <Input name="password" onChange={this.handleFieldChange} placeholder="Пароль" label="Введите Ваш пароль" type="password"/>
                            <Button onClick={ (e) => { e.preventDefault();return this.handleAuth()}} loading={this.props.auth.loading}>Авторизация</Button>
                        </Form>
                    </Wrapper>

                }
            
        </div>
         );
    }
}
 
const mapDispatchToProps = dispatch => ({
    login: (user)=>dispatch(actions.login(user)),
    reset: () => dispatch(actions.authReset())

}) 

const mapStateToProps = state =>({
    isLogged:state.user.isLogged,
    auth:state.user.authState
})
 
export default connect(mapStateToProps,mapDispatchToProps)(AuthForm);