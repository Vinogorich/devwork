import React, { useState, useEffect,Fragment } from "react";
import Label from "../Label/Label";
import css from "./ProductData.module.scss";
import filterCss from "./_Filter.module.scss";
import GoogleMapReact from "google-map-react";
import SearchResultsMapItem from '../SearchResults/_SearchResultsMapItem';
import ImageGallery from "react-image-gallery";
import "../../assets/scss/image-gallery.scss";
import util from "../util/util";
import ProductsFilter from "../ProductsFilter/ProductsFilter";
import ProductItem from './_ProductItem';
import Reviews from '../Reviews/Reviews';
import {connect} from 'react-redux';
import * as actions from '../../store/actions/index';

const ProductData = props => {
    useEffect( () => {
        const images = document.getElementsByTagName("img");
        if (images) {
            for (let img of images) {
                img.onerror = () => {
                    if (img.src.indexOf('localhost') !== -1) {
                        img.src = img.src.replace(/http:\/\/localhost:[\d]+/,"https://test.uniqtrip.ru");
                    }
                }
            }

        }

        }
     
    )
    const slides = props.photos.map(item => ({
        original: item.mid,
        thumbnail: item.small
    }));
    const labels = [];
    const data = props.data;
    const services = [];
    const [dateIn,  setDateIn] = useState(util.moment().add(1, "day"));
    const [dateOut, setDateOut] = useState(util.moment().add(7, "day"));
    const [mapVisible, mapToggle] = useState(false);

    let main_review = "";
    if (props.params.stars) {
        labels.push(
            <Label>
                {props.params.stars} <i className="ut-icon-star1"></i>
            </Label>
        );
    }

    if (props.params && props.params.services) {
        services.push(
            ...props.params.services.map((item, index) => (
                <li key={index}>
                    <i className={"ut-icon-"+item.class}></i>
                    {item.title}
                </li>
            ))
        );
    }

    if (props.params && props.params.reviews) {
        main_review = (
            <div className={css["main-review"]}>
                <img src={props.params.reviews[0].image} />
                <div>
                    <h6>{props.params.reviews[0].title}</h6>
                    <p>{util.getText(props.params.reviews[0].title, 120)}</p>
                </div>
            </div>
        );
    }
    let products = []
    if (props.products) {
        products = props.products.map((item,index)=>{
            const itemData = {
                productId:item.id,
                categoryId:item.category_id,
                dateIn:util.moment().add(20,'day').format("YYYY-MM-DD"),
                dateOut:util.moment().add(27,'day').format("YYYY-MM-DD"),
                adults:2,
                children:0, 
                time:'10:00:00',
                Fname:'',
                Lname:'',
                email:'',
            };
            return (
            <ProductItem key={index} data={item} addHandler={()=>props.addToCart(itemData)}/>)
        }
        )
    }

    let position = util.getPosition(props.data.params.position);
    const categoryCode = props.category;
    
    const marker = position ? 
                    <SearchResultsMapItem 
                        data={{...props.data, categoryCode}}
                        {...position}
                        categoryCode
                    >
                        <i className='ut-icon-home1'></i>
                    </SearchResultsMapItem>
                    :
                    null

    return (
        <section className="ut-section__container ut-theme-color">
            <div className={css["main"]}>
                <div className={css["gallery"]}>
                    {
                        mapVisible && marker ?
                        <GoogleMapReact
                        bootstrapURLKeys={{
                            key: "AIzaSyBk0yr_pnl0TRs9oQy0j6-qkOqMJR3WLwU"
                        }}
                        defaultCenter={{
                            ...position
                        }}
                        defaultZoom={10}
                        yesIWantToUseGoogleMapApiInternals
                        >
                            {marker}
                        </GoogleMapReact>

                        :
                    <ImageGallery
                        items={slides}
                        lazyLoad={true}
                        showThumbnails={false}
                        fillParent={true}
                    />
                    }
                </div>
                <div className={css["data"]}>
                    <h1 className={css["title"]}>{data.name}</h1>
                    <div>{labels}</div>
                    <p className={util.join(css["location"], mapVisible && position && css['location--on'] )} onClick={()=>mapToggle(!mapVisible)}>
                        <i className="ut-icon-geo1"></i>
                        <a>{props.params.address}</a>
                    </p>
                    <ul className={css["services"]}>{services.slice(0,7)}</ul>
                    {main_review}
                </div>
            </div>
            {
                props.products ?
                    <Fragment>
                        <div className={css["filter"]}>
                            <h4 className="ut-section__title">Выберите предложение</h4>
                            <ProductsFilter
                                css={filterCss}
                                dateIn={dateIn}
                                dateOut={dateOut}
                            />
                        </div>
                        <div className={css['products']}>
                            {products}
                        </div>
                    </Fragment>
                    :
                    null
            }
            <Reviews 
                {...props.reviews}
            />




            {
                props.params.services_desc 
                ?
                <div className={css['text']}>
                    <h3 className={css['text-title']}>
                        {
                            props.params.services_desc_title ? 
                            props.params.services_desc_title :
                            "Описание"
                        }
                        </h3>
                    <div className={css['text-content']}>
                        {util.getHTML(props.params.services_desc)}
                    </div>
                </div>
                :
                null
            }
            {
                props.params.terms 
                ?
                <div className={css['text']}>
                    <h3 className={css['text-title']}>
                        {
                            props.params.terms_title ?
                            props.params.terms_title :
                            "Условия оказания услуги"
                        }
                    </h3>
                    <div className={css['text-content']}>
                    {util.getHTML(props.params.terms)}
                    </div>
                </div>
                :
                null
            }
            {
                props.params.note
                ?
                <div className={css['text']}>
                    <h3 className={css['text-title']}>
                        {
                        props.params.note_title ?
                        props.params.note_title :
                        "Примечания*"
                        }
                    </h3>
                    <div className={css['text-content']}>
                    {util.getHTML(props.params.note)}
                    </div>
                </div>
                :
                ""
            }
        </section>
    );
};


const mapStateToProps = state => ({
    cart: state.user.cart
});

const mapDispatchToProps = dispatch => ({
    addToCart:(item)=>dispatch(actions.addToCart(item))

});

export default connect(mapStateToProps,mapDispatchToProps)(ProductData);


