import React from "react";
import css from "./ProductItem.module.scss";
import LikeSwitch from "../LikeSwitch/LikeSwitch";
import util from "../util/util";
import { createObject } from '../../model/Favourites';

const ProductItem = props => {
    /* let img;

        if ( props.data.params && props.data.params.photos ) {
            img = props.data.params.photos[0].small;
        } else {
            img=dummy;
        } */
    const img = util.getPropertySafe(props.data,"params.photos.0.small",util.dummyImage().small);

    const itemData = createObject(
        props.data.id,
        props.data.name,
        img,
        props.data.category_id,
        props.data.id
    );
    
    return (
        <div className={css["container"]}>
            <LikeSwitch itemData={itemData}/>
            <img
                className={css["image"]}
                src={img}
            />
            <div className={css["description"]}>
                <h6 className={css["title"]}>{props.data.name}</h6>
                <p className={css["text"]}>
                     {util.getText(props.data.services_desc, 999)}
                </p>
            </div>
            <div className={css["prices"]}>
                <p className={css["price"]}>
                    {util.currencyFormat(parseInt(props.data.price) > 0 ? props.data.price : 0)}
                </p>
                <p className={css["hint"]}>
                    {props.data.category_id == 7 ?
                    "стоимость за сутки"
                    :
                    null   
                }
                </p>
                <div className={css['buttons']}>
                    <button
                    onClick={props.addHandler}
                        className={`${css["btn"]} ${css["btn-icon"]} ut-theme-color`}
                    >
                        <i className="ut-icon-cart1"></i>
                    </button>
                    &nbsp;
                    <button className={`${css["btn"]}   ut-theme-color`} onClick={props.addHandler}>
                        <span>Забронировать</span>
                    </button>
                </div>
            </div>
        </div>
    );
};

export default ProductItem;
