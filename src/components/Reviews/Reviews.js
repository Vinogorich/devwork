import React from 'react';
import css from './Reviews.module.scss';
import PropTypes from 'prop-types'; 
import util from '../util/util';
import Label from '../Label/Label';

const Reviews = props => {   
    if (props.items.length < 1) return null;
    const totalsItems = props.totals.parameters.map((item,index) => (
        <li key={index} className={css['totals-item']}>
            <div className={css['totals-item-text']}>
                <p>{item.title}</p>
                <p>{item.value}</p>
            </div>
            <div className={css['totals-item-gauge']}>
                <span style={{width:(item.value*10)+'%'}}/>
            </div>
        </li>
    ));

    const reviewItems = props.items.map( (item,index) => (
        <li className={css['item']}>
            <div>
                <img src={item.img} className={css['item-img']}/>

            </div>
            <div>
                <p className={css['item-name']}>
                    {item.name}
                </p>
                <p className={css['item-title']}>
                    <Label>{item.rate}</Label>&nbsp;
                    {item.title}
                </p>
                <p className={css['item-text']}>
                    {item.text}
                </p>

            </div>
        </li>

    ));

    return (  
        <div className={css['container']}>
                <h3 className={css['title']}>
                    Отзывы гостей
                </h3>
            <div className={css['flex']}>
                <div className={ util.join(css['totals-col'], "ut-theme-color" ) }>
                    <div className={css['totals-main']}>
                        <div className={css['totals-rate']}>
                            {parseFloat(props.totals.rate)}
                        </div>
                        <div>
                            <p className={css['totals-title']}>
                                {props.totals.title}
                            </p>
                            <p className={css['totals-quantity']}>
                                {props.totals.quantity} отзывов
                            </p>
                        </div>
                    </div>
                    <ul className={css['totals-items']}>
                        {totalsItems}
                    </ul>
                </div>
                <ul className={css['items-col']}>
                    {reviewItems}
                </ul>
            </div>
        </div>

    );
}

Reviews.propTypes = {
    items  : PropTypes.array.isRequired,
    totals : PropTypes.object.isRequired,
}

Reviews.defaultProps = {
    items:[],
    totals:{}
}
 
export default Reviews;