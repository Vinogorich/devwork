import React, { Component } from "react";
import classes from "./Header.module.scss";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import util from "../util/util";
import img from "../../assets/image/dummy_small.jpg";

import Wrapper from "../../hoc/Wrapper/Wrapper";
import UserForm from "../UserForm/UserForm";
import * as actions from "../../store/actions/index";

class Header extends Component {
    state = {
        formVisible: false,
        panelVisible: false
    };

    componentDidMount() {
        this.props.login();
        this.props.getCart();
        this.props.getCategories();
    }

    formToggle = () => {
        this.setState(state => ({
            ...state,
            formVisible: !state.formVisible
        }));
    };

    panelToggle = () => {
        this.setState(state => ({
            ...state,
            panelVisible: !state.panelVisible
        }));
    };

    render() {
        const form = this.state.formVisible ? (
            <UserForm
                authHandler={this.authHandler}
                hideHandler={this.formToggle}
            />
        ) : null;
        const profileIcon = this.props.isLogged ? (
            <Link to="/profile">
                <img
                    src={util.getPropertySafe(
                        this.props.user,
                        "profile.image",
                        img
                    )}
                />
            </Link>
        ) : (
            <i className="ut-icon-person" onClick={this.formToggle}></i>
        );

        const panelCategories = this.props.catalog.categories.map(
            (category, index) => {
                const checked =
                    Boolean(this.props.category) &&
                    this.props.category.id == category.id;
                return (
                    <li
                        onClick={() => this.props.setCategory(category)}
                        key={index}
                        className={util.join(
                            classes["panel-category"],
                            checked && classes["panel-category--on"]
                        )}
                    >
                        <i className={"ut-icon-" + category.icon}></i>
                        <span>{category.name}</span>
                    </li>
                );
            }
        );

        const panel = (
            <div
                className={util.join(
                    classes["panel"],
                    this.state.panelVisible && classes["panel--on"]
                )}
                onClick={e => {
                    if (e.target.classList.contains(classes["panel"])) {
                        this.panelToggle();
                    }
                }}
            >
                <div className={classes["panel-content"]}>
                    <ul className="ut-theme-color">{panelCategories}</ul>
                </div>
            </div>
        );

        const dayOfTheYear = util.moment().format("DDD");
        return (
            <Wrapper>
                {panel}
                {form}
                <div className={classes["wrapper"]}>
                    <header className={classes["header"]}>
                        <div className={classes["logo"]}>
                            <span
                                onClick={this.panelToggle}
                                className={[
                                    classes["logo-link"],
                                    "ut-theme-color"
                                ].join(" ")}
                            >
                                ☰
                            </span>
                            &nbsp;
                            <Link
                                to="/"
                                className={[
                                    classes["logo-link"],
                                    "ut-theme-color",
                                    (dayOfTheYear > 360 || dayOfTheYear < 14) &&
                                        classes["logo--ny"]
                                ].join(" ")}
                            >
                                <span>Uniq</span>Trip
                            </Link>
                        </div>
                        <nav className={classes["menu"]}>
                            <ul>
                                <li className="v-desktop">
                                    <a
                                        className={[
                                            classes["menu-link"],
                                            "ut-theme-color",
                                            "ut-brand"
                                        ].join(" ")}
                                        href="#club"
                                    >
                                        Uniq<span>Trip</span>Club
                                    </a>
                                </li>
                                <li className="v-desktop">
                                    <a
                                        className={[
                                            classes["menu-link"],
                                            classes["menu-link-simple"]
                                        ].join(" ")}
                                        href="#b2b"
                                    >
                                        Для Бизнеса
                                    </a>
                                </li>
                                {/*                                 <li className="v-desktop">
                                    <a className={[classes["menu-link"],classes["menu-link-simple"]].join(' ')} href="#deferred">Рассрочка и Кредит</a>
                                </li> */}
                                <li className="v-desktop">
                                    <a
                                        className={[
                                            classes["menu-link"],
                                            classes["menu-link-black"]
                                        ].join(" ")}
                                        href="tel:+78625552530"
                                    >
                                        +7 862 555 25 30
                                    </a>
                                </li>
                                <li>
                                    <Link
                                        to="/cart"
                                        className={[
                                            classes["menu-link"],
                                            classes["menu-icon"],
                                            "ut-theme-color"
                                        ].join(" ")}
                                    >
                                        <i className="ut-icon-cart1"></i>
                                        {this.props.user.cart.positions > 0 ? (
                                            <sub>
                                                <span>
                                                    {
                                                        this.props.user.cart
                                                            .positions
                                                    }
                                                </span>
                                            </sub>
                                        ) : null}
                                    </Link>
                                </li>
                                <li className={classes["profile"]}>
                                    {profileIcon}
                                </li>
                            </ul>
                        </nav>
                    </header>
                </div>
            </Wrapper>
        );
    }
}

const mapStateToProps = state => ({
    isLogged: state.user.isLogged,
    user: state.user,
    catalog: state.catalog,
    category: state.user.customerData.category
});

const mapDispatchToProps = dispatch => ({
    login: () => dispatch(actions.login()),
    getCart: () => dispatch(actions.getCart()),
    getCategories: () => dispatch(actions.getCategories()),
    setTerm: term => dispatch(actions.setTerm(term)),
    setCategory: category => dispatch(actions.setCategory(category))
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);
