import React, { Fragment, useState } from "react";
import css from "./SearchResults.module.scss";
import util from "../util/util";
import { Link } from "react-router-dom";
import Label from "../Label/Label";

const SearchResultsMapItem = props => {
    const [balloonOn, toggleBalloon] = useState(false);
    const labels = [];
    if (util.getPropertySafe(props, "data.params.stars")) {
        labels.push(
            <li key={"stars"}>
                <Label>
                    {props.data.params.stars} <i className="ut-icon-star1"></i>
                </Label>
            </li>
        );
    }

    if (util.getPropertySafe(props, "data.params.rating")) {
        labels.push(
            <li key={"rating"}>
                <Label>{props.data.params.rating}</Label>
            </li>
        );
    }

    return (
        <Fragment>
            <div
                onClick={() => toggleBalloon(!balloonOn)}
                className={util.join(css["map-item"], "ut-theme-color")}
            >
                {props.children}
            </div>

            {balloonOn ? (
                <div className={css["map-balloon"]}>
                    {labels.length > 0 ? <ul>{labels}</ul> : null}

                    <img
                        src={props.data.params.photos[0].small}
                        onClick={() => toggleBalloon(false)}
                    />
                    <Link
                        to={`/catalog/${props.data.categoryCode}/${props.data.code}`}
                    >
                        {props.data.name}
                    </Link>
                </div>
            ) : null}
        </Fragment>
    );
};

export default SearchResultsMapItem;
