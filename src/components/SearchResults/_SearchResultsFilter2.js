import React, { useEffect, useState, useCallback } from "react";
import css from "./styles/Filter.module.scss";
import style from "../TimePicker/styles/TimePicker.module.scss";
import { isBrowser } from "react-device-detect";
import { Link } from "react-router-dom";

import Litepicker from "litepicker";
import "../../assets/scss/litepicker.scss";

import MorePicker from "../TimePicker/MorePicker";
import TimePicker from "../TimePicker/TimePicker";
import CategoryChart from "../TimePicker/CategoryChart";

import util from "../util/util";
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";

import * as routes from "../util/routes";
import color from "@material-ui/core/colors/amber";
import SortIcon from "@material-ui/icons/Sort";
import { grey } from "@material-ui/core/colors";

let picker;

const SearchResultsFilter = props => {
  const [pickerVisible, setPickerVisible] = useState(false);

  const fieldDateIn = React.createRef();
  const fieldDateOut = React.createRef();

  const datePick = (dateIn, dateOut) => {
    const _dateOut = dateOut ? dateOut : util.moment(dateIn).add(1, "day");

    props.setFilter({
      dateIn,
      dateOut: _dateOut
    });
  };

  const filterConfig = useCallback(param => {
    let config = {};

    // If no category selected, or accommodation is selected -
    // double date picker,
    // else - single date

    config.singleMode = !(props.category.id == 7);
    config.datePickerClassName = config.singleMode
      ? util.join(css["input-text-inline"], css["input-text"])
      : css["input-text"];
    //config.datePickerPlaceholder = getDateText(config.singleMode);
    config["field1_label"] = config.singleMode ? "Когда" : "Заезд";
    config["field2_label"] = config.singleMode ? "Во сколько" : "Выезд";
    if (param && config.hasOwnProperty(param)) {
      return config[param];
    }
    return config;
  });

  const toggleSort = () => {
    const sort = props.sort;
    props.setSort(sort == "ASC" ? "DESC" : "ASC");
  };

  useEffect(() => {
    const config = filterConfig();
    const input_dateIn = fieldDateIn.current;
    const input_dateOut = fieldDateOut.current;
    if (picker) picker.destroy();

    picker = new Litepicker({
      element: input_dateIn,
      elementEnd: config.singleMode ? null : input_dateOut,
      singleMode: config.singleMode,
      numberOfMonths: 2,
      format: "DD.MM.YYYY",
      numberOfColumns: 2,
      minDate: util.moment().add(1, "day"),
      lang: "ru-RU",
      tooltipText: {
        one: "день",
        few: "дня",
        many: "дней"
      },
      onSelect: function(start, end) {
        const _end = end || util.moment(start).add(1, "day");

        props.setFilter({
          dateIn: start,
          dateOut: _end
        });
      }
    });
  }, [fieldDateIn, fieldDateOut, filterConfig, props, props.category]);

  // const subCategories = props.category.subcat.map((item, index) => (
  //   <option key={index} value="item.name">
  //     {item.name}
  //   </option>
  // ));

  const dateIn = props.filter.dateIn;
  const dateOut = props.filter.dateOut || props.filter.dateIn;
  const categoryUrl = routes.CATEGORY_PREFIX + props.category.code;

  const time_options = [];
  for (let i = 0; i < 24; i++) {
    time_options.push(<option key={i} value={`${i}:00`}>{`${i}:00`}</option>);
  }
  // const time_select = (
  //   <select
  //     name="time"
  //     className={css["filter-select"]}
  //     value={props.filter.time}
  //     onChange={e =>
  //       props.setFilter({
  //         time: e.target.value
  //       })
  //     }
  //   >
  //     {time_options}
  //   </select>
  // );

  const dateOut_input = (
    <input
      readOnly
      value={
        filterConfig("singleMode")
          ? props.filter.time
          : dateOut.format("DD.MM.YYYY")
      }
      name="hotel_date_out"
      className={css["filter-input"]}
      ref={fieldDateOut}
    />
  );

  // const inputRef = useRef();
  // useImperativeHandle(props.ref, () => ({
  // 	focus: () => {
  // 		inputRef.current.setPickerVisible(false);
  // 	}
  // }));
  // const seterPickerViseble = () => {
  //   setPickerVisible(false);
  // };
  //logic for Category
  let defaultValue =
    typeof props.filter.subCategory === "string"
      ? "Все"
      : props.filter.subCategory.name;
  let nameSubcategories = props.category.subcat.map(el => {
    return el.name;
  });

  let chart = (
    <CategoryChart
      arraySubCategory={nameSubcategories}
      defaultValue={defaultValue}
      subCategories={props.category.subcat}
      subCategory={props.filter.subCategory}
      // parentState={props.parentState}
      // setParentState={props.setParentState}
      // clickCategory={(val) => { clickCategory(val) }}
      test={true}
      pickHandler={sub => {
        setPickerVisible(false);
        props.setFilter({
          subCategory: sub
        });
      }}
      hideHandler={() => setPickerVisible(false)}
    />
  );

  let classes = ["ut-theme-color", css["filter"]];
  if (props.setter) {
    classes.push(css["activeBtn"]);
  }
  // if (!props.setter) {
  //   classes.pop();
  // }
  return (
    <div className={classes.join(" ")}>
      <Link className={util.join(css["filter-title"])} to={categoryUrl}>
        {props.category.name}
      </Link>
      <div
        onClick={() => setPickerVisible(!pickerVisible)}
        className={css["filter-group"]}
      >
        <i
          className={util.join(
            "ut-icon-" + props.category.icon,
            css["filter-icon"]
          )}
        ></i>
        <div>
          <label className={css["filter-label"]}>Тип</label>

          {/* CategoryPicker */}
          <p className={style["input-category"]}>{defaultValue}</p>
          {pickerVisible ? chart : null}
          {/* CategoryPicker */}
        </div>
      </div>
      <div className={css["filter-group"]}>
        <i className={"ut-icon-calendar_1 " + css["filter-icon"]}></i>
        <div>
          <label className={css["filter-label"]}>
            {filterConfig("field1_label")}
          </label>
          <input
            readOnly
            value={dateIn.format("DD.MM.YYYY")}
            name="date_in"
            className={css["filter-input"]}
            ref={fieldDateIn}
          />
        </div>
        <div> - </div>
        <div>
          <label className={css["filter-label"]}>
            {filterConfig("field2_label")}
          </label>
          {filterConfig("singleMode") ? (
            <TimePicker
              more={true}
              time={props.filter.time}
              pickHandler={(h, m) => props.setFilter({ time: h + ":" + m })}
              notTime={false}
            />
          ) : (
            dateOut_input
          )}
        </div>
      </div>
      <div className={css["filter-group"]}>
        <i className={`ut-icon-guest ${css["filter-icon"]}`}></i>
        <div>
          <label className={css["filter-label"]}>Гостей</label>

          <MorePicker
            adults={props.filter.adults}
            more={true}
            main={false}
            children={props.filter.children}
            pickHandler={(ad, ch) => {
              props.setFilter({
                adults: parseInt(ad)
              });
              props.setFilter({
                children: parseInt(ch)
              });
            }}
          />
        </div>
      </div>
      <div className={css["filter-group"]}>
        {/* <i
          className={util.join("ut-icon-", "SortIcon", css[""])}
        ></i> */}

        <SortIcon style={{ color: grey[400] }} />
        <div>
          <label className={css["filter-label"]}>Сортировка</label>
          <p className={css["filter-text"]} onClick={toggleSort}>
            {props.sort == "ASC" ? "По возрастанию" : "По убыванию"}
          </p>
        </div>
      </div>
      <button
        className={util.join("ut-theme-color", css["filter-btn"])}
        onClick={props.onSearchClick}
      >
        <span>Поиск</span>
      </button>
    </div>
  );
};

const mapStateToProps = state => ({
  catalog: state.catalog,
  sort: state.user.customerData.sort,
  filter: state.user.customerData.filter
});

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(actions.setFilter(filter)),
  setSort: sort => dispatch(actions.setSort(sort))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResultsFilter);
