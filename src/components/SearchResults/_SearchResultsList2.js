import React, { Component } from "react";
import SearchResultItem from "./_SearchResultItem";
import css from "./SearchResults.module.scss";
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";
import util from "../util/util";

class SearchResultsList extends Component {
    render() {
        const items = this.props.products.map((item, index) => {
            const itemData = {
                productId: item.params.productid,
                categoryId: item.category_id,
                dateIn: this.props.filter.dateIn.format("YYYY-MM-DD"),
                dateOut: this.props.filter.dateOut.format("YYYY-MM-DD"),
                adults: this.props.filter.adults,
                children: this.props.filter.children,
                time: this.props.filter.time,
                Fname: "",
                Lname: "",
                email: ""
            };
            return (
                <SearchResultItem
                    category={this.props.category}
                    data={item}
                    key={index}
                    addToCart={() => {
                        this.props.addToCart(itemData);
                    }}
                />
            );
        });
        return (
            <div className={css["data"]}>
                <ul className={css["list"]} ref={this.props.scrollRef}>
                    {items
                        ? items
                        : "На запрошенный период предложения не найдены"}
                    <li></li>
                </ul>
                <button
                    className={css["btn"]}
                    onClick={this.props.onMoreBtnClick}
                >
                    Еще
                </button>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    cart: state.user.cart,
    filter: state.user.customerData.filter
});

const mapDispatchToProps = dispatch => ({
    addToCart: item => dispatch(actions.addToCart(item))
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchResultsList);
