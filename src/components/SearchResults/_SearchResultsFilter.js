import React, { useEffect, useState, useCallback } from "react";
import css from "./SearchResults.module.scss";
import style from "../TimePicker/styles/TimePicker.module.scss";
import { isBrowser } from "react-device-detect";
import { Link } from "react-router-dom";
import "lightpick/css/lightpick.css";
import * as routes from "../util/routes";

// import Lightpick from "lightpick";
import Litepicker from "litepicker";
import MorePicker from "../TimePicker/MorePicker";
import CategoryChart from "../TimePicker/CategoryChart";
import SortIcon from "@material-ui/icons/Sort";
import { grey } from "@material-ui/core/colors";

import util from "../util/util";
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";
import TimePicker from "../TimePicker/TimePicker";
let picker;
const SearchResultsFilter = props => {
  const ref1 = React.createRef();
  const ref2 = React.createRef();

  const fieldDateIn = React.createRef();
  const fieldDateOut = React.createRef();

  const datePick = (dateIn, dateOut) => {
    const _dateOut = dateOut ? dateOut : util.moment(dateIn).add(1, "day");

    props.setFilter({
      dateIn,
      dateOut: _dateOut
    });
  };

  const filterConfig = useCallback(param => {
    let config = {};

    // If no category selected, or accommodation is selected -
    // double date picker,
    // else - single date

    config.singleMode = !(props.category.id == 7);
    config.datePickerClassName = config.singleMode
      ? util.join(css["input-text-inline"], css["input-text"])
      : css["input-text"];
    //config.datePickerPlaceholder = getDateText(config.singleMode);
    config["field1_label"] = config.singleMode ? "Когда" : "Заезд";
    config["field2_label"] = config.singleMode ? "Во сколько" : "Выезд";
    if (param && config.hasOwnProperty(param)) {
      return config[param];
    }
    return config;
  });
  useEffect(() => {
    const config = filterConfig();
    const input_dateIn = fieldDateIn.current;
    const input_dateOut = fieldDateOut.current;
    if (picker) picker.destroy();

    picker = new Litepicker({
      element: input_dateIn,
      elementEnd: config.singleMode ? null : input_dateOut,
      singleMode: config.singleMode,
      numberOfMonths: 2,
      format: "DD.MM.YYYY",
      numberOfColumns: 2,
      minDate: util.moment().add(1, "day"),
      lang: "ru-RU",
      tooltipText: {
        one: "день",
        few: "дня",
        many: "дней"
      },
      onSelect: function(start, end) {
        const _end = end || util.moment(start).add(1, "day");

        props.setFilter({
          dateIn: start,
          dateOut: _end
        });
      }
    });
  }, [fieldDateIn, fieldDateOut, filterConfig, props, props.category]);
  //state for CategoryPiker
  const [pickerVisible, setPickerVisible] = useState(false);

  // useEffect(() => {
  //   const field1 = ref1.current;
  //   const field2 = ref2.current;

  //   const config = {
  //     field: field1,
  //     minDate: util.moment().add(1, "day"),
  //     secondField: field2,
  //     singleDate: false,
  //     onSelect: datePick
  //   };
  //   new Lightpick(config);
  // }, [datePick, ref1, ref2]);

  // const subCategories = props.subCategories.map((item, index) => (
  // 	<option key={index} value="item.name">
  // 		{item.name}
  // 	</option>

  // ));

  const dateIn = props.filter.dateIn;
  const dateOut = props.filter.dateOut || props.filter.dateIn;
  const categoryUrl = routes.CATEGORY_PREFIX + props.category.code;

  const time_options = [];
  for (let i = 0; i < 24; i++) {
    time_options.push(<option key={i} value={`${i}:00`}>{`${i}:00`}</option>);
  }

  const dateOut_input = (
    <input
      readOnly
      value={
        filterConfig("singleMode")
          ? props.filter.time
          : dateOut.format("DD.MM.YYYY")
      }
      name="hotel_date_out"
      className={css["filter-input"]}
      ref={fieldDateOut}
    />
  );

  // let fakeCategory = "Все";
  const toggleSort = () => {
    const sort = props.sort;
    props.setSort(sort == "ASC" ? "DESC" : "ASC");
  };
  //logic for CategoryPicker
  let defaultValue =
    typeof props.filter.subCategory === "string"
      ? "Все"
      : props.filter.subCategory.name;
  let nameSubcategories = props.subCategories.map(el => {
    return el.name;
  });
  let chart = (
    <CategoryChart
      arraySubCategory={nameSubcategories}
      defaultValue={defaultValue}
      subCategories={props.subCategories}
      subCategory={props.filter.subCategory}
      test={true}
      pickHandler={sub => {
        setPickerVisible(false);
        props.setFilter({
          subCategory: sub
        });
      }}
      hideHandler={() => setPickerVisible(false)}
      // pickHandler={sub => {
      // 	setPickerVisible(false);
      // 	// props.setParentState(false);
      // 	props.pickHandler(sub);
      // }}
    />
  );
  return (
    <div className={`${css["filter"]} ut-theme-color`}>
      <Link
        className={util.join("ut-section__title", css["filter-title"])}
        to={"/category/acc"}
      >
        {props.title}
      </Link>

      <div
        onClick={() => setPickerVisible(!pickerVisible)}
        className={css["filter-group"]}
      >
        <i
          className={util.join(
            "ut-icon-" + props.category.icon,
            css["filter-icon"]
          )}
        ></i>
        <div>
          <label className={css["filter-label"]}>Жилье</label>
          {/* CategoryPicker */}
          <p className={style["input-category"]}>{defaultValue}</p>
          {pickerVisible ? chart : null}
          {/* CategoryPicker */}
        </div>
      </div>
      <div className={css["filter-group"]}>
        <i className={"ut-icon-calendar_1 " + css["filter-icon"]}></i>
        <div>
          <label className={css["filter-label"]}>
            {filterConfig("field1_label")}
          </label>
          <input
            readOnly
            value={dateIn.format("DD.MM.YYYY")}
            name="date_in"
            className={css["filter-input"]}
            ref={fieldDateIn}
          />
        </div>
        <div> - </div>
        <div>
          <label className={css["filter-label"]}>
            {filterConfig("field2_label")}
          </label>
          {filterConfig("singleMode") ? (
            <TimePicker
              more={true}
              time={props.filter.time}
              pickHandler={(h, m) => props.setFilter({ time: h + ":" + m })}
              notTime={false}
            />
          ) : (
            dateOut_input
          )}
        </div>
      </div>
      {/* <div className={css["filter-group"]}>
        <i className={"ut-icon-calendar_1 " + css["filter-icon"]}></i>
        <div>
          <label className={css["filter-label"]}>Заезд</label>
          <input
            readOnly
            value={dateIn.format("DD.MM.YYYY")}
            name="hotel_date_in"
            className={css["filter-input"]}
            ref={ref1}
          />
        </div>
        <div> - </div>
        <div>
          <label className={css["filter-label"]}>Выезд</label>
          <input
            readOnly
            value={dateOut.format("DD.MM.YYYY")}
            name="hotel_date_out"
            className={css["filter-input"]}
            ref={ref2}
          />
        </div>
      </div> */}
      <div className={css["filter-group"]}>
        <i className={`ut-icon-guest ${css["filter-icon"]}`}></i>
        <div>
          <label className={css["filter-label"]}>Гостей</label>
          <MorePicker
            adults={props.filter.adults}
            product={false}
            more={true}
            main={false}
            children={props.filter.children}
            pickHandler={(ad, ch) => {
              props.setFilter({
                adults: parseInt(ad)
              });
              props.setFilter({
                children: parseInt(ch)
              });
            }}
          />
        </div>
        <div>
          <label className={css["filter-label"]}>&nbsp;</label>
        </div>
      </div>
      <div className={css["filter-group"]}>
        {/* <i
          className={util.join(
            "ut-icon-" + props.category.icon,
            css["filter-icon"]
          )}
        ></i> */}
        <SortIcon style={{ color: grey[400] }} />
        <div>
          <label className={css["filter-label"]}>Сортировка</label>
          <p className={css["filter-text"]} onClick={toggleSort}>
            {props.sort == "ASC" ? "По возрастанию" : "По убыванию"}
          </p>
        </div>
      </div>
      <button
        className={util.join("ut-theme-color", css["filter-btn"])}
        onClick={props.onSearchClick}
      >
        <span>Поиск</span>
      </button>
    </div>
  );
};

const mapStateToProps = state => ({
  catalog: state.catalog,
  sort: state.user.customerData.sort,
  filter: state.user.customerData.filter
});

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(actions.setFilter(filter)),
  setSort: sort => dispatch(actions.setSort(sort))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResultsFilter);
