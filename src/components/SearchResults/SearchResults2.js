import React, { Component, Fragment } from "react";
import SearchResultsFilter from "./_SearchResultsFilter2";
import SearchResultsList from "./_SearchResultsList2";
import SearchResultsButton from "./_SearchResultsButton";
import css from "./SearchResults.module.scss";
import style from "./styles/Filter.module.scss";
import { isMobileOnly } from "react-device-detect";
import GoogleMapReact from "google-map-react";
import { connect } from "react-redux";
import util from "../util/util";
import withLoading from "../../hoc/Loading/Loading";
import SearchResultsMapItem from "./_SearchResultsMapItem";
import api from "../API/new/API";
import * as actions from "../../store/actions/index";

const defaultCategoryId = 7;

class SearchResults extends Component {
  state = {
    data: [],
    finished: false,
    pressed: false
  };

  /**
   *  Update component when catalog data is available
   *
   */

  componentDidMount() {
    this.props.loading(true);
    this._getCategoriesData();
  }

  componentDidUpdate(prevProps) {
    if (
      this.props.catalog.length !== prevProps.catalog.length ||
      this.props.term !== prevProps.term
    ) {
      this._getCategoriesData();
    }
  }

  /**
   *
   * getMoreProducts on button
   *
   *
   */

  loadMore = () => {
    const _currentCategoryId = this.props.category.id;
    const _currentCategoryData = this.state.data.filter(
      item => item.category.id == _currentCategoryId
    )[0];

    if (!_currentCategoryData) return null;
    const requestParams = this.getRequestParams(_currentCategoryData);
    api.search.SearchPartners(requestParams).then(response => {
      const items = util.getPropertySafe(response, "data.items", []);
      if (items.length > 0) {
        const newItems = [..._currentCategoryData.items, ...items];

        this.setState(state => {
          const data = [...state.data];

          for (let i = 0; i < data.length; i++) {
            if (data[i].category.id === _currentCategoryData.category.id) {
              data[i].items = [...newItems];
            }
          }
          return {
            data
          };
        });
      }
    });
  };

  getRequestParams(currentCategoryData) {
    util.log(currentCategoryData);
    return {
      categoryId: currentCategoryData.category.id,
      dateIn: this.props.filter.dateIn.format("YYYY-MM-DD"),
      dateOut: this.props.filter.dateOut.format("YYYY-MM-DD"),
      adults: this.props.filter.adults,
      children: this.props.filter.children,
      time: this.props.filter.time,
      term: this.props.term,
      offset: currentCategoryData.items ? currentCategoryData.items.length : 0,
      sort: this.props.sort,
      subcat: this.props.filter.subCategory.id
    };
  }

  /**
   *
   * Fetch Products by every category in catalog
   *
   */

  _getCategoriesData = () => {
    this.setState(
      state => ({
        ...state,
        data: [],
        finished: false
      }),
      () => {
        this.props.loading(true);

        const _currentCategoryId = this.props.category
          ? this.props.category.id
          : defaultCategoryId;

        const categories = this.props.catalog.sort((a, b) => {
          if (a.id == _currentCategoryId) return -1;
          if (b.id == _currentCategoryId) return 1;
          return 0;
        });

        const term = this.props.term;
        const updatePromises = [];
        categories.forEach(category => {
          const requestParams = this.getRequestParams({ category });

          const updatePromise = api.search.SearchPartners(requestParams);

          updatePromises.push(updatePromise);
          updatePromise.then(response => {
            const items = util.getPropertySafe(response, "data.items");
            const totalItems = util.getPropertySafe(
              response,
              "data.totalItems",
              0
            );

            if (items && items.length > 0) {
              this.props.loading(false);

              this.setState(state => {
                const data = [...state.data];
                const newItem = {
                  category,
                  total: totalItems,
                  items
                };
                data.push(newItem);
                return {
                  ...state,
                  data
                };
              });
            }
          });
        });
        Promise.all(updatePromises).then(() => {
          this.setState(state => ({
            ...state,
            finished: updatePromises.length > 0
          }));
          this.props.loading(false);
        });
      }
    );
  };

  prepareData() {
    let _currentCategoryId = this.props.category
      ? this.props.category.id
      : defaultCategoryId;

    let _currentCategoryData = this.state.data.filter(
      item => item.category.id == _currentCategoryId
    )[0];

    if (!_currentCategoryData) {
      if (this.state.data.length == 0) {
        return null;
      } else {
        _currentCategoryData = this.state.data[0];
        _currentCategoryId = _currentCategoryData.category.id;
      }
    }

    const buttons = this.state.data.map((item, index) => (
      <SearchResultsButton
        key={index}
        selected={item.category.id == _currentCategoryId}
        onClick={() => this.props.setCategory(item.category)}
      >
        {item.category.name} {item.total > 0 ? `(${item.total})` : null}
      </SearchResultsButton>
    ));

    const items = _currentCategoryData ? [..._currentCategoryData.items] : [];

    const _mapElements = _currentCategoryData.items
      .filter(item => util.getPosition(item.params.position))
      .map((item, index) => (
        <SearchResultsMapItem
          key={index}
          data={{ ...item, categoryCode: this.state.categoryCode }}
          lat={item.params.position.split(",")[0]}
          lng={item.params.position.split(",")[1]}
        >
          <i className="ut-icon-home1"></i>
        </SearchResultsMapItem>
      ));

    const map = !isMobileOnly ? (
      <div className={css["map"]}>
        <div className={css["map-container"]}>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: "AIzaSyBk0yr_pnl0TRs9oQy0j6-qkOqMJR3WLwU"
            }}
            defaultCenter={{
              lat: 43.6606049,
              lng: 40.0118365
            }}
            defaultZoom={10}
            yesIWantToUseGoogleMapApiInternals
          >
            {_mapElements}
          </GoogleMapReact>
        </div>
      </div>
    ) : null;

    return { buttons, items, map, category: { ..._currentCategoryData } };
  }
  handlerPressed() {
    this.setState({
      pressed: !this.state.pressed
    });
  }
  render() {
    const data = this.prepareData();

    const message = data
      ? null
      : this.state.finished
      ? "Ничего не найдено"
      : "поиск...";

    if (message)
      return (
        <Fragment>
          <div className={css["main"]}>{message}</div>
        </Fragment>
      );

    return (
      <Fragment>
        <div className={css["wrapper-btn-mobile"]}>
          {/* <div
            onClick={() => this.handlerPressed()}
            className={css["btn-more"]}
          >
            Еще
          </div> */}
          {data.buttons}
        </div>
        <div></div>
        <div className={css["wrapper-btn"]}>{data.buttons}</div>
        <SearchResultsFilter
          // handler={this.handlerPressed}
          setter={this.state.pressed}
          category={{ ...data.category.category }}
          onSearchClick={this._getCategoriesData}
        />
        <div className={css["main"]}>
          <SearchResultsList
            category={data.category}
            onMoreBtnClick={this.loadMore}
            products={data.items}
          />
          {data.map}
        </div>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  catalog: state.catalog.categories,
  category: state.user.customerData.category,
  term: state.user.customerData.searchTerm,
  filter: state.user.customerData.filter,
  sort: state.user.customerData.sort
});
const mapDispatchToProps = dispatch => ({
  setCategory: category => {
    dispatch(actions.setCategory(category));
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withLoading(SearchResults));
