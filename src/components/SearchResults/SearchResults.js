import React, { Component } from "react";
import SearchResultsFilter from "./_SearchResultsFilter";
import SearchResultsList from "./_SearchResultsList";
import css from "./SearchResults.module.scss";
import { isMobileOnly } from "react-device-detect";
import GoogleMapReact from "google-map-react";
import { connect } from "react-redux";
import apiDev from "../API/API.dev";
import util from "../util/util";
import withLoading from "../../hoc/Loading/Loading";
import SearchResultsMapItem from "./_SearchResultsMapItem";
import { Link } from "react-router-dom";
import * as actions from "../../store/actions/index";
import Categories from "../../model/Categories";

const defaultCategory = {
  id: 7,
  code: "acc",
  name: "Проживание",
  icon: "home",
  subcat: []
};

class SearchResults extends Component {
  state = {
    subCategories: [],
    productsList: [],
    offset: 0,
    scrollRef: React.createRef()
  };

  componentDidMount() {
    this.props.loading(true);
    this.loadMore();
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.category != prevProps.category) {
      this.props.loading(true);
      const newState = {
        ...this.state,
        productsList: [],
        offset: 0
      };

      this.setState({ ...newState }, this.loadMore);
    }
  }

  getCurrentCategory() {
    return (
      this.props.category ||
      new Categories(this.props.categories).byId(7) ||
      defaultCategory
    );
  }

  datePickHandler = (dateIn, dateOut) => {
    dateOut = dateOut ? dateOut : this.state.dateOut;
    this.setState(
      state => ({
        ...state,
        dateIn,
        dateOut,
        offset: 0,
        productsList: []
      }),
      this.loadMore
    );
  };

  guestPickHandler = (adults, children) => {
    if (adults && adults > 0) {
      this.setState(state => ({
        ...state,
        adults: parseInt(adults)
      }));
    } else {
      this.setState(state => ({
        ...state,
        children: parseInt(children)
      }));
    }
  };

  loadMore = () => {
    const currentCategory = { ...this.getCurrentCategory() };
    const params = {
      offset: this.state.offset,
      categoryId: currentCategory.id,
      dateIn: this.props.filter.dateIn.format("YYYY-MM-DD"),
      dateOut: this.props.filter.dateOut.format("YYYY-MM-DD"),
      adults: this.props.filter.adults,
      children: this.props.filter.children,
      time: this.state.time,
      subcat: this.props.filter.subCategory.id
    };

    apiDev("search/BestProducts", params).then(response => {
      if (
        !util.objectsEqual(currentCategory, this.props.category) &&
        this.props.category
      ) {
        return;
      }
      this.props.loading(false);
      if (response.data && Array.isArray(response.data)) {
        const productsList = this.state.productsList.concat(response.data);

        this.setState(
          state => ({
            ...state,
            productsList,
            offset: this.state.offset + response.data.length
          }),
          () => {
            const e = this.state.scrollRef.current;
            e.scrollTop = e.scrollHeight;
          }
        );
      }
    });
  };

  render = () => {
    const currentCategory = this.getCurrentCategory();
    const mapElements = this.state.productsList.map((item, index) => {
      const position = util.getPosition(
        util.getPropertySafe(item, "params.position")
      );
      const price = parseInt(util.getPropertySafe(item, "params.price"));
      const content =
        price > 0 ? (
          <span>{util.currencyFormat(price)}</span>
        ) : (
          <i className={`ut-icon-${currentCategory.icon}`}></i>
        );

      return (
        <SearchResultsMapItem
          key={index}
          data={{ ...item, category: { ...currentCategory } }}
          {...position}
        >
          {content}
        </SearchResultsMapItem>
      );
    });

    const map = !isMobileOnly ? (
      <div className={css["map"]}>
        <div className={css["map-container"]}>
          <GoogleMapReact
            bootstrapURLKeys={{
              key: "AIzaSyBk0yr_pnl0TRs9oQy0j6-qkOqMJR3WLwU"
            }}
            defaultCenter={{
              lat: 43.6606049,
              lng: 40.0118365
            }}
            defaultZoom={10}
            yesIWantToUseGoogleMapApiInternals
          >
            {mapElements}
          </GoogleMapReact>
        </div>
      </div>
    ) : (
      ""
    );

    const filter = !isMobileOnly ? (
      <SearchResultsFilter
        title={currentCategory.name}
        subCategories={currentCategory.subcat}
        onGuestPick={this.guestPickHandler}
        onDatePick={this.props.setFilter}
        dateIn={this.props.filter.dateIn}
        dateOut={this.props.filter.dateOut}
        adults={this.props.filter.adults}
        children={this.props.filter.children}
        category={this.getCurrentCategory()}
      />
    ) : (
      <Link className="ut-section__title" to={"/category/acc"}>
        {this.state.categoryTitle}
      </Link>
    );

    return (
      <section>
        <div className="ut-section__container">
          {filter}
          <div className={css["main"]}>
            <SearchResultsList
              filter={{ ...this.props.filter }}
              scrollRef={this.state.scrollRef}
              category={this.getCurrentCategory()}
              onMoreBtnClick={this.loadMore}
              products={this.state.productsList}
            />
            {map}
          </div>
        </div>
      </section>
    );
  };
}

const mapStateToProps = state => ({
  catalog: state.catalog,
  filter: state.user.customerData.filter,
  category: state.user.customerData.category,
  categories: state.catalog.categories
});

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(actions.setFilter(filter))
});

export default connect(mapStateToProps)(withLoading(SearchResults));
