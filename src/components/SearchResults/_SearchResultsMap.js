import React from 'react';
import css from "./SearchResults.module.scss";
const  crypto = require( "crypto-js/md5"); 

const  SearchResultsMap= (props) => {
    let key = crypto(new Date());
    let field_id = `input_${key}`;
    return ( 
        <div className={css["map-container"]} id={field_id}>
        </div> 
    );
}
 
export default SearchResultsMap;