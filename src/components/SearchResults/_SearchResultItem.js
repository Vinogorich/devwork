import React from 'react';
import css from './SearchResults.module.scss';
import {Link} from 'react-router-dom';
import LikeSwitch from '../LikeSwitch/LikeSwitch';
import Label from '../Label/Label';
import util from '../util/util';



const SearchResultItem = (props) => {
    
    const labels = [];
    if (props.data.params.stars) {
        labels.push(
            <Label key="stars">
                {props.data.params.stars} <i className="ut-icon-star1"></i>
            </Label>
        );
    }
     
    if (Array.isArray(props.data.params.labels)) {
        props.data.params.labels.map( (item,index) => {

            labels.push(
                <Label key={index} color={item.class}>
                    {item.title}
                </Label>
            )
        })
    }

    
    const price = props.data.params.price ? 
        (util.currencyFormat(props.data.params.price))
        :
        ""
        ;
    const _url = `${window.location.href}catalog/${props.category.code}/${props.data.code}`;
    const _title = props.data.name;
    const _image = util.getPropertySafe( props.data, 'params.photos.0.small', util.dummyImage().small );

    const itemData = {
        id:props.data.id,
        name: props.data.name,
        image:_image,
        categoryCode:props.category.code,
        partnerCode:props.data.code,
    }
    const vkLink = `https://vk.com/share.php?url=${_url}&title=${_title}&image=${_image}&noparse=true`
    const partnerLink = `/catalog/${props.category.code}/${props.data.code}`;

    return ( 
        <li className={`${css['item']} ut-theme-color`}>
        <LikeSwitch itemData={itemData} />
        <ul className={`${css['item-socials']}`}>
            <li><a href={vkLink} target="_blank"><i className="ut-icon-vk1"></i></a></li>
            <li><a href="#share"><i className="ut-icon-whatsapp1"></i></a></li>
            <li><a href="#share"><i className="ut-icon-instagram1"></i></a></li>
            <li><a href="#share"><i className="ut-icon-twitter1"></i></a></li>
            <li><a href="#share"><i className="ut-icon-facebook1"></i></a></li>
        </ul>
        <Link to={partnerLink}  style={{backgroundImage: `url('${_image}')`}} alt="" className={`${css['item-img']}`} />
        
        <div className={`${css['item-data']}`} >
        <Link to={partnerLink}  className={`${css['item-link']}`}>
                
            </Link>
            <Link to={partnerLink}  className={`${css['item-title']}`}>
                {props.data.name}
            </Link>
            <div>
                {labels}
                
            </div>
            <div className={`${css['item-text']}`}>
            {props.data.text}
            </div>

            <div className={`${css['item-price']}`}>
                {price}
            </div>
            <div className={`${css['item-hint']}`}>
                {
                    props.data.category_id == 7 ?
                    "стоимость указана за сутки"
                    :
                    null
                }
            </div>
            <div className={`${css['item-buttons']}`}>
                <button onClick={props.addToCart} className={`${css['item-btn']} ${css['item-btn-icon']} ut-theme-color`}>
                    <i className="ut-icon-cart1"></i>
                </button>&nbsp;
                <Link to={partnerLink} className={`${css['item-btn']} ut-theme-color`}>
                    <span>Забронировать</span>
                </Link>

            </div>


        </div>
    </li>

     );
}
 
export default SearchResultItem;