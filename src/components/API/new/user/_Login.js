import util from "../../../util/util"
import img from '../../../../assets/image/dummy_small.jpg';
import { createObject } from '../../../../model/User';

/* const userObject = {
    firstName:'',
    lastName: '',
    gender:"f",
    image: img,
    email:"",
    phone:"",
    id:'',
    social: {
        vk: "",
        facebook: "",
        twitter:"",
        instagram:"",
        ok:"",
    }
};

const userFactory = {
    createUser: (user)=> {
        const _user = {
            firstName:user.fname,
            lastName: user.lname,
            gender:user.sex && user.sex.substr(0,1).toLowerCase()==="m" ? "m" : "f",
            image: user.img && user.img.length > 5 ? user.img : null,
            email: user.email,
            phone: user.phone,
            id: user.id,
        }
        return util.mergeObjects(userObject,_user);
    }
} */

const config = {
    methodName:'Login',
    isTest:false,
    isDummy:false,
    method:'POST',
    dummyData:{
        firstName:'Мария',
        lastName: 'Петрова',
        gender:"f",
        image: "https://www.rd.com/wp-content/uploads/2017/09/01-shutterstock_476340928-Irina-Bg-1024x683.jpg",
        email:"mary@me.com",
        phone:"+79480010203",
        id:'004',
        social: {
            vk: "https://vk.com/id45281251",
            facebook: "https://www.facebook.com/petrova.maria1"
        }
    },
    prepareRequest: (p)=> {
        return {
                email: p.email,
                password:p.password
            
        }
    },
    prepareResponse: (r)=> {
        if (!r.error) {
            r.user = createObject(r);
        }
        return r;
    },
    handleError: (e) => e        
}

export default config; 


