import  GetProductReviews from './_GetProductReviews';
import GetSubCategory from './_GetSubCategory';
import GetDiscounts from './_GetDiscounts';

export default {
    GetProductReviews,
    GetSubCategory,
    GetDiscounts
} 
