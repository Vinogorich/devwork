import man_img from '../../../../assets/image/man-profile.jpg';
import woman_img from '../../../../assets/image/woman-profile.jpg';

const config = {
    methodName:'GetProductReviews',
    isTest:false,
    isDummy:true,
    method:'get',
    dummyData: {
        totals: {
            rate:9.1,
            quantity:267,
            title:'Превосходно',
            parameters: [
                {
                    title:'Расположение',
                    value:8.9
                },
                {
                    title:'Персонал',
                    value:8.4
                },
                {
                    title:'Чистота',
                    value:9.6
                },
                {
                    title:'Комфорт',
                    value:8.2
                },
                {
                    title:'Соотношение цена/качество',
                    value:9.1
                },
                
            ]
        },
        items:[
        {
            id:0,
            title:'Превосходно',
            img: man_img,
            name:'Сергей Скворцов',
            text: 'Замечательное расположение. Очень красивый вид из окон номера. Все в номере очень чисто и уютно. Даже есть очаровательный комплимент от отеля . Когда мы приехали нас радушно встретили и принесли кофе и пирог. Очень приятно! Нам понравилось и мы обязательно приедем в этот уютный и комфортный отель  !!!!',
            rate: 9.2

        },
        {
            id:1,
            title:'Мой лучший отдых',
            img: woman_img,
            name:'Анна Пирогова',
            text: 'Тихий, по домашнему уютный отель. Очень клиентоориентирован, сотрудники внимательны к каждому гостю. Хороший ресторан, приятным бонусом стал концерт живой музыки. Обязательно сюда вернусь и буду рекомендовать друзьям и знакомым. Спасибо за комфортный отдых.',
            rate: 8.3

        }

    ]},

    prepareRequest: (p)=> p,
        
    prepareResponse: (r)=> r.data,
    
    handleError: (e) => e        
}

export default config; 


