import GetCart from "./_GetCart";
import AddToCart from "./_AddToCart";
import AddOrder from "./_AddOrder";
import RemoveFromCart from "./_RemoveFromCart";
import QuickOrder from "./_QuickOrder";

export default {
    GetCart,
    AddToCart,
    AddOrder,
    QuickOrder,
    RemoveFromCart
};
