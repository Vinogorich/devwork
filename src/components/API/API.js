import axios from "axios";
import interfaces from './Interfaces.dev';

const api = (str,data = {}) => {
    const url = "https://dev.uniqtrip.ru/rest";
    const _axios = axios.create({
        baseURL: url
    });
    const params = str
        .replace(/(^\/|\s|\/&)/g, "")
        .toLowerCase()
        .split("/");
    /* let queryString ='';    

    if (data ) {
        queryString = Object.keys(data).map(key => key + '=' + data[key]).join('&');
    } */

    if (params.length !== 3) {
        throw new Error(`Wrong number of arguments found in ${str}`);
    }

    const interfaceName = params[0],
        itemName = params[1],
        actionName = params[2];
    let requestUrl = interfaces[interfaceName][itemName][actionName];
    if (!requestUrl)  {
        throw new Error( `Invalid method/action called`);
    }
    const query = {
        params: {...data},
        action : actionName
    }
    //requestUrl += queryString;
    return _axios.post(requestUrl,query);
};

export default api;
