import axios from "axios";


const api = (element) => {
    const url = "https://dev.uniqtrip.ru/data/";
    const _axios = axios.create({
        baseURL: url
    });
    
    const queryUrl =`get.php?element=${element}`;
    return _axios.get(queryUrl);
};

export default api;

export const categoryMappingKP = {
    "hotels"   : 'проживание',
    "food"     : "питание",
    "events"   : "События",
    "children" : "Отдых с детьми",
    "beauty"   : "Спа и красота",
};

export const labelMapping = {
    inhotel:"Рестораны в отелях",
    restaurant:"Рестораны на курорте",
    barslobby:"Лобби-бары",
    cafebar:"Кафе и Бары",
    karaoke:"Караоке",
    apreski:"Апре-ски",
    nightclub:"Ночные клубы",
    streetfood:"Фастфуд",
    delivery:"Доставка",
    bbq:"BBQ",
}