const development = {
  api_url: "https://test.uniqtrip.ru/rest/"
};
const production = {
  // api_url: "https://dev.uniqtrip.ru/rest/"
  api_url: "https://shop.uniqtrip.ru/rest/"
};
const test = {
  ...development
};
const _default = {
  ...development
};

let config = { ..._default };

switch (process.env.NODE_ENV) {
  case "development":
    config = { ...development };
    break;
  case "test":
    config = { ...test };
    break;
  case "production":
    config = { ...production };
    break;
}

export default config;
