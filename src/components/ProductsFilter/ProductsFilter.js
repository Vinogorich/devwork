import React, { useEffect, useState } from "react";
import "lightpick/css/lightpick.css";
import Lightpick from "lightpick";
// import MorePicker from "../TimePicker/MorePicker";
import TimeChart from "../TimePicker/TimeChart";
import style from "../TimePicker/styles/TimePicker.module.scss";
import util from "../util/util";
import { connect } from "react-redux";

import * as actions from "../../store/actions/index";

const SearchResultsFilter = props => {
  const ref1 = React.createRef();
  const ref2 = React.createRef();
  useEffect(() => {
    const field1 = ref1.current;
    const field2 = ref2.current;

    const config = {
      field: field1,
      secondField: field2,
      singleDate: false,
      onSelect: props.onDatePick,
      minDate: util.moment()
    };
    new Lightpick(config);
  }, [props.onDatePick, ref1, ref2]);

  const subCategoriesHTML = props.subCategories ? (
    <div className={props.css["filter-group"]}>
      <i className={"ut-icon-home2 " + props.css["filter-icon"]}></i>
      <div>
        <label className={props.css["filter-label"]}>Жилье</label>
        <select name="hotel_type" className={props.css["filter-select"]}>
          {props.subCategories.map((item, index) => (
            <option key={index} value="item.name">
              {item.name}
            </option>
          ))}
        </select>
      </div>
    </div>
  ) : (
    ""
  );
  //logic for  more picker

  let [pickerVisible, setPickerVisible] = useState(props.toggleVisible);
  let sum = () => {
    if (props.filter.adults < 2) {
      return " взрослый";
    } else {
      return " взрослых";
    }
  };
  let sumChild = () => {
    if (props.filter.children < 1) {
      return "без детей";
    } else if (props.filter.children == 1) {
      return props.filter.children + " ребенок";
    } else {
      return props.filter.children + " детей";
    }
  };
  // product = { true}
  // more = { true}
  // main = { false}
  let chart = (
    <TimeChart
      // adult={true}
      more={true}
      adults={props.filter.adults}
      pickHandler={(ad, ch) => {
        setPickerVisible(false);
        props.setFilter({
          adults: parseInt(ad)
        });
        props.setFilter({
          children: parseInt(ch)
        });
      }}
      hideHandler={() => setPickerVisible(!pickerVisible)}
      // child={true}
      children={props.filter.children}
      // pickHandlerChild={ad => {
      //   setPickerVisible(false);
      //   props.pickHandlerChild(ad);
      // }}
    />
  );
  let main = false;
  let product = true;
  let classes = [style["moreWrapper"]];

  if (product) {
    classes.push(style.productAdults);
  }
  return (
    <div className={`${props.css["filter"]} ut-theme-color`}>
      {props.title ? (
        <a className="ut-section__title" href="#">
          {props.title}
        </a>
      ) : (
        ""
      )}

      {subCategoriesHTML}

      <div className={props.css["filter-group"]}>
        <i className={"ut-icon-calendar1 " + props.css["filter-icon"]}></i>
        <div>
          <label className={props.css["filter-label"]}>Заезд</label>
          <input
            readOnly
            value={props.dateIn.format("DD.MM.YYYY")}
            name="hotel_date_in"
            className={props.css["filter-input"]}
            ref={ref1}
          />
        </div>
      </div>
      <div className={props.css["filter-group"]}>
        <i className={"ut-icon-calendar1 " + props.css["filter-icon"]}></i>

        <div>
          <label className={props.css["filter-label"]}>Выезд</label>
          <input
            readOnly
            value={props.dateOut.format("DD.MM.YYYY")}
            name="hotel_date_out"
            className={props.css["filter-input"]}
            ref={ref2}
          />
        </div>
      </div>
      <div className={props.css["filter-group"]}>
        <i className={"ut-icon-person " + props.css["filter-icon"]}></i>
        <div className={props.css["wrappMore"]}>
          <label
            className={`${props.css["filter-label"]} ${props.css["label-more"]} `}
          >
            Гостей
          </label>
          {/* <select className={props.css["filter-select"]} defaultValue="2">
                                <option value="1">1 взрослый</option>
                                <option value="2">2 взрослых</option>
                                <option value="3">3 взрослых</option>
                                <option value="4">4 взрослых</option>
                                <option value="5">5 взрослых</option>
                            </select> */}
          {/* <MorePicker
            // adults={props.filter.adults}
            // product={true}
            // more={true}
            // main={false}
            // children={props.filter.children}
            // pickHandler={(ad, ch) => {
            //   props.setFilter({
            //     adults: parseInt(ad)
            //   });
            //   props.setFilter({
            //     children: parseInt(ch)
            //   });
            // }}
          /> */}
          <div className={classes.join(" ")}>
            <p
              onClick={() => setPickerVisible(!pickerVisible)}
              // value={props.adults + sum()}
              // readOnly
              className={
                main ? style["picker-input"] : style["input-subfilter"]
              }
            >
              {props.filter.adults + sum()}
            </p>
            {/* <input
				type="text" */}
            <p
              onClick={() => setPickerVisible(!pickerVisible)}
              // value={sumChild()}
              // readOnly
              className={
                main ? style["picker-input"] : style["input-subfilter"]
              }
            >
              {sumChild()}
            </p>
          </div>
          {pickerVisible ? chart : null}
        </div>
        {/* <div>
                            <label className={props.css["filter-label"]}>&nbsp;</label>
                            <select type="text" name="children" className={props.css["filter-select"]} defaultValue="0" >
                                <option value="0">без детей</option>
                                <option value="1">1 ребенок</option>
                                <option value="2">2 детей</option>
                                <option value="3">3 детей</option>
                                <option value="4">4 детей</option>
                                <option value="5">5 детей</option>
                            </select>
                    </div> */}
      </div>
      <button className={props.css["filter-btn"]}>
        <span>Изменить параметры поиска</span>
      </button>
    </div>
  );
};
const mapStateToProps = state => ({
  catalog: state.catalog,
  filter: state.user.customerData.filter
});

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(actions.setFilter(filter))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchResultsFilter);
