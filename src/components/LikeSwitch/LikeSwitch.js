import React, { Component } from 'react';
import css from './LikeSwitch.module.scss';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import util from '../util/util';

class LikeSwitch extends Component {
    getState = () => {
        return this.props.favourites.some(
            (e) => (e.id ===this.props.itemData.id)
        );
    }

    toggleState = ()=> {
        return this.getState() ? this.props.removeFavourite (this.props.itemData) : this.props.addFavourite (this.props.itemData) ;  
    }

    render =()=> { 
        const enabled = this.getState();
        const className = enabled ? util.join("ut-icon-heart1", css['like--on']) : "ut-icon-heart-border1";
        return (  
            <a className={`${css['like']}`} onClick={this.toggleState}>
                <label> <i className={className}></i>  </label>
            </a>
        );
    }
}
 

const mapStateToProps = state => ({
    favourites:state.user.favourites
});

const mapDispatchToProps = dispatch => ({
    addFavourite: (itemData) => dispatch(actions.addFavourite(itemData)),
    removeFavourite: (itemData) => dispatch(actions.removeFavourite(itemData)),
});


export default connect(mapStateToProps,mapDispatchToProps)(LikeSwitch);