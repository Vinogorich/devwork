import React from 'react';
import css from './Footer.module.scss';

const Footer = () => (
    
<footer className={`${css['footer']}`}>
    <div className="ut-section__container ut-theme-color">
        <div className={`${css['col']}`}>
            <div className={`${css['sub-col']}`}>
                <a href="/">Uniq<span>Trip</span></a>
            </div>
            <div className={`${css['sub-col']}`}>
                    <a href="/">Главная</a>
                    <a href="/">Партнеры</a>
                    <a href="/">Контакты</a>
                    <a href="/">B2B</a>
            </div>
            
            <div className={`${css['sub-col']}`}>
                    <a href="/">Главная</a>
                    <a href="/">Партнеры</a>
                    <a href="/">Контакты</a>
                    <a href="/">B2B</a>
            </div>
            
            <div className={`${css['sub-col']}`}>
                    <a href="/">Главная</a>
                    <a href="/">Партнеры</a>
                    <a href="/">Контакты</a>
                    <a href="/">B2B</a>
            </div>
            
            <div className={`${css['sub-col']}`}>
                    <a href="/">Главная</a>
                    <a href="/">Партнеры</a>
                    <a href="/">Контакты</a>
                    <a href="/">B2B</a>
            </div>
            

        </div>
        <div className={`${css['col']}`}>
            <div className={`${css['sub-col-big']} v-mobile`}>
                <a href="/">Uniq<span>Trip</span></a>
            </div>
            <div className={`${css['sub-col-big']}`}>
                <form action="/subscribe" method="GET" className={`${css['form']}`}>
                    <input type="text" className={`${css['input']}`} placeholder="Ваш Email"/>
                    <button className={`${css['input-btn']}`}><i className="ut-icon-nav1"></i></button>
                    <p className={`${css['hint']}`}>
                        Подпишитесь на новости и спецпредложения
                    </p>
                </form>
            </div>
            <div className={`${css['sub-col-big']}`}>
                <ul className={`${css['socials']}`}>
                    <li><a href="https://facebook.com"> <i className="ut-icon-facebook1"></i>      </a></li>
                    <li><a href="https://facebook.com"> <i className="ut-icon-facebook1"></i>      </a></li>
                    <li><a href="https://facebook.com"> <i className="ut-icon-facebook1"></i>      </a></li>
                    <li><a href="https://facebook.com"> <i className="ut-icon-facebook1"></i>      </a></li>
                </ul>
            </div>

        </div>

    </div>
</footer>
        

)

export default Footer;
