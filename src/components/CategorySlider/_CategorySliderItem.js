import React from 'react';
import css from './CategorySlider.module.scss';
import Label from '../Label/Label';
import LikeSwitch from '../LikeSwitch/LikeSwitch';
import util from '../util/util';
import { Link, withRouter } from 'react-router-dom';
import { createObject } from '../../model/Favourites';
import img from '../../assets/image/dummy_small.jpg';

const CategorySliderItem = (props) => {
    let categories = [];
    if (props.item.category) {
        categories = props.item.category.split(",").map( (item,index) => {
            return (
                <li key={index}>

                </li>
            )
        });

    }
    const labels = [];
    const _photo = util.getPropertySafe(props.item,"params.photos.0.small",img);
    if (props.item.params.avgcheck) {
        labels.push(
            <Label key={"avgcheck"} color="green">Средний чек {util.currencyFormat(props.item.params.avgcheck)}</Label>
        )
    }

    const itemData = createObject(
        props.item.id,
        props.item.name,
        _photo,
        props.item.categoryCode,
        props.item.code
    );

    return (
        <li className={css['block']} style={{backgroundImage:`url(${props.item.params.photos[0].small})`}}>
        <LikeSwitch itemData={itemData}/>
        <Link to={`/catalog/${props.category.code}/${props.item.code}`} className={css['block-img-link']}></Link>
        <div className={css['block-data']}>
                <ul className={css['block-labels']}>
                        {labels}
                        
                </ul>
                { props.item.params.price ?
                    <div className={css['block-price']}><span>от</span> { util.currencyFormat( props.item.params.price) }</div>
                    :
                    ""
                }
                <div className={css['block-container']}>
                    <Link to={`/catalog/${props.category.code}/${props.item.code}`}>
                        <h6 className={css['block-title']}>
                            {util.entitiyDecoder( props.item.name )}
                        
                        </h6>
                        <ul className={css['block-tags']}>
                            {categories}
                        </ul>
                        <ul className={css['block-features']}>
                            {/* <li>использование локальных продуктов</li>
                            <li>раки в различной варке или жарке</li>
                            <li>доставка по территории Горки Город </li> */}
                            { util.getText(props.item.params.services_desc,160 ) }
                        </ul>
                    </Link>    
                    <a className={css['block-review']}>
                        {/* <img src="media/profile1.jpg" alt="" />
                        <p>Очень атмосферно, есть парковка для посетителей, очень вкусные огуречные ...</p> */}
                    </a>
                </div>
                <div className={css['block-buttons']}>
                        {/* <button className={`${css['block-btn']} ${css['block-btn-icon']} ut-theme-color`} >
                                <i className="ut-icon-cart1"></i>
                            </button> */}
                            {
                            }
                            <Link to={`/catalog/${props.category.code}/${props.item.code}`} className={`${css['block-btn']} ut-theme-color`}>
                                <span>{
                                    props.item.params.price || props.category.id == 7 ? "Забронировать" : "Подробнее"}</span>
                            </Link>
                            {/* <button  class="ut-blocks__block-btn ut-theme-color">
                                <span>меню</span>
                            </button> */}
                </div>
            
        </div>

    </li>

    );
}
 
export default withRouter(CategorySliderItem);