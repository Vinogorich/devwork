import React, { Component } from "react";
import css from './CategorySlider.module.scss';
import CategorySliderItem from './_CategorySliderItem';
import { Link } from 'react-router-dom';
import apiDev from '../API/API.dev';
import util from "../util/util";
import withLoading from '../../hoc/Loading/Loading';

/**
 * Mobile Detect
 */

const gridSize = () => {

    const SMALL = 399;
    const MED = 767;
    const GRID = {
        small:1,
        med:2,
        large:3
    }
    return window.innerWidth <= SMALL ?
        GRID.small 
        :
        ( window.innerWidth <= MED ?
            GRID.med
            :
            GRID.large
        )
    ;
}

class CategorySlider extends Component {
    
    state = {
        items:[],
        visibleItems:[],
        offset:0,
        end:false,
        loadSize: gridSize(),
    }
    
    componentDidMount = () => {
        
        this.loadMore();
        window.addEventListener('resize', ()=>{
            const loadSize = gridSize();
            this.setState(state=>({
                ...state,
                loadSize
            }))
        });
    }

    loadMore = (cb) => {

        const params = {
            categoryId:this.props.category.id,
            offset:this.state.items.length
        }
        if (this.props.subCategory) {
            params.subcategoryId = this.props.subCategory.id
        }
        this.props.loading(true);
        apiDev('catalog/GetPartnersByCategory',params)
            .then( response => {
                this.props.loading(false);
                if (response.data && Array.isArray(response.data)) {
                    const items = this.state.items.concat( response.data );
                    
                    this.setState ( state => ({
                        ...state,
                        //end: (response.data.length < 10),
                        items,    
                    }),
                    cb
                    );
                } else {
                    this.setState ( state => ({
                        ...state,
                        end:true
                    }));
                }
            })
    }

    scrollLeft = () => {
        const loadSize = this.state.loadSize;
        let offset = this.state.offset;
        if (offset == 0 ) return;
         const newOffset = offset-loadSize < 0 ? 0 : offset-loadSize;
        offset = newOffset+loadSize;
            this.setState(state=>({
                ...state,
                offset: newOffset,
                end:false
            })
        )
    }

    scrollRight = () => {
        const loadSize = this.state.loadSize;
        if (this.state.offset + loadSize >= this.state.items.length) {
            this.loadMore( ()=>{
                this.setState(state=>({
                    ...state,
                    offset:state.offset + loadSize
                }))
            });
        }
        else {
            const maxOffset = this.state.items.length - loadSize;
            const newOffset = this.state.offset + loadSize
            this.setState(state=>({
                ...state,
                offset: newOffset < maxOffset ? newOffset : maxOffset
            }))
        }

    }


    render = () => {

        const items = this.state.items.slice(this.state.offset,this.state.offset+this.state.loadSize).map((item,index) => (
            <CategorySliderItem idx={index} key={index} item={item} category={this.props.category}/>   
        ))
        if (items.length == 0 ) return null;
        return (
            <section>
                <div className="ut-section__container ut-theme-color">
                    <Link to={`/category/${this.props.category.code}`} className="ut-section__title">
                        {
                            this.props.subCategory ?
                            this.props.subCategory.name
                            :
                            this.props.category.name
                        }
                        
                    </Link>
                    <button 
                        onClick={this.scrollLeft}
                        className={`${css['controls']} ${css['controls-left']}`}
                        disabled={this.state.offset <= 0 }
                    >
                        ❬
                    </button>
                    <button 
                        onClick={this.scrollRight}
                        className={`${css['controls']} ${css['controls-right']}`}
                        disabled={this.state.end}
                    >
                        ❭
                    </button>
                    <ul className={css['container']}>
                        {items}
                    </ul>
                </div>
            </section>
        );
    }
}

export default withLoading(CategorySlider);
