import React from "react";
import css from "./Interface.module.scss";
import util from "../util/util";
import Puff from "react-preloader-icon/loaders/Puff";
import PreloaderIcon from "react-preloader-icon";

export const Container = props => {
    return (
        <div className={util.join(css["container"], props.className)}>
            {props.children}
        </div>
    );
};

export const Form = props => {
    return <form className={props.className}>{props.children}</form>;
};

export const Input = props => {
    return (
        <div className="ut-form__group">
            {props.label ? (
                <label className="ut-form__label">{props.label}</label>
            ) : (
                ""
            )}
            <input
                type={props.type ? props.type : "text"}
                /* placeholder={props.placeholder}
            defaultValue={props.defaultValue}
            onChange={props.onChange} */
                {...props}
                className={"ut-form__input"}
            />
        </div>
    );
};

export const Button = props => {
    const r = props.loading ? (
        <div className="ut-form__preloader">
            <PreloaderIcon
                loader={Puff}
                size={32}
                strokeWidth={8}
                strokeColor="#666"
                duration={800}
            />
        </div>
    ) : (
        <button
            className="ut-theme-color ut-form__button-inverse"
            onClick={props.onClick}
        >
            <span>{props.children}</span>
        </button>
    );

    return r;
};

export const Panel = props => {
    return <div className={css["panel"]}>{props.children}</div>;
};
