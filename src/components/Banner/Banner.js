import React from 'react';
import img from '../../assets/image/banner1.jpg';
const Banner = () => {

    return ( <section>
    <div className="ut-section__container ut-theme-color">
        <div className="ut-banner" style={{ backgroundImage : `url(${img})`}}>
            

            <ul className="ut-banner__socials ut-theme-color">
                    <li><a href="#share"><i className="ut-icon-vk1"></i></a></li>
                    <li><a href="#share"><i className="ut-icon-whatsapp1"></i></a></li>
                    <li><a href="#share"><i className="ut-icon-instagram1"></i></a></li>
                    <li><a href="#share"><i className="ut-icon-twitter1"></i></a></li>
                    <li><a href="#share"><i className="ut-icon-facebook1"></i></a></li>
            </ul>

            <div className="ut-banner__data">

                <a href="/" className="ut-banner__title">
                    Ж/Д Билеты выгодно!
                </a>
                <a href="/" className="ut-banner__slogan">
                    Билет со скидкой 10%
                </a>
                <a href="/" className="ut-banner__text">
                    ЖД билеты на поезд онлайн. 
                    Все способы оплаты.
                    Удобный выбор мест!
                </a>
                <div className="ut-banner__buttons">
                    <a href="/" className="ut-banner__button ut-theme-color">
                        <span>Купить билет</span>
                    </a>
                </div>

            </div>
        </div>
    </div>
</section>


    );
}
 
export default Banner;
