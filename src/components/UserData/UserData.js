import React from 'react';
import css from './UserData.module.scss';
import util from '../util/util';

const UserData = props => {
    return (
        <div className={css["wrapper"]}>
            <div className={css["col-image"]}>
                <img src={props.profile.image} />
            </div>
            <div className={css["col-info"]}>
                <p className={css['name']}>
                    {props.profile.firstName} {props.profile.lastName}                   
                </p>
                <p className={css['text']}>
                    Email: {props.profile.email}
                </p>
                <p className={css['text']}>
                    Телефон: {props.profile.phone}
                </p>
            </div>
            <div className={util.join(css["col-menu"],"ut-theme-color")}>
                <ul className={css['menu-container']}>
                    <li className={css['menu-item']}>
                        <i className="ut-icon-calendar1"></i> Мое Избранное
                    </li>
                    <li className={css['menu-item']}>
                        <i className="ut-icon-calendar1"></i> Мой календарь и события
                    </li>
                    <li className={css['menu-item']}>
                        <i className="ut-icon-calendar1"></i> Мои покупки
                    </li>
                    <li className={css['menu-item']}>
                        <i className="ut-icon-calendar1"></i> Сообщество
                    </li>
                    <li className={css['menu-item']} onClick={props.logout}>
                        <i className="ut-icon-more"></i> Выйти
                    </li>
                </ul>
            </div>

        </div>
    
    );
}
 
export default UserData;