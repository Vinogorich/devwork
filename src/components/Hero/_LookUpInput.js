import React, { Component } from "react";
// import { withRouter } from "react-router";
import { connect } from "react-redux";
import css from "./styles/Lookup.module.scss";
import apiDev from "../API/API.dev";
import loadingImg from "../../assets/image/loading.gif";
import * as actions from "../../store/actions/index";
import { Link } from "react-router-dom";
import * as routes from "../util/routes";
import util from "../util/util";

class LookUpInput extends Component {
  state = {
    lookupItems: [],
    lookupTerm: "",
    lookupPayload: [],
    value: "",
    isLoading: false
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.category != prevProps.category) {
      this.setState(
        state => ({
          ...state,
          lookupItems: [],
          lookupPayload: [],
          lookupTerm: ""
        }),
        this.lookup
      );
    }
  }

  onLookupItemClick = item => {
    this.setState(
      state => ({
        ...state,
        lookupItems: [],
        value: item.title
      }),
      () => {
        if (item.partnerId > 0) {
          this.props.valueHandler(item);
          this.props.setTerm(item.title);
        }
      }
    );
  };

  getEmptyListItem = () => {
    return {
      partnerId: -1,
      title: "...",
      partnerCode: null,
      categoryCode: null,
      subtitle: "Больше результатов"
    };
  };

  lookup = e => {
    const term = e ? e.target.value : "";
    this.props.setTerm(term);
    const value = e
      ? this.state.isLoading
        ? this.state.value
        : e.target.value
      : this.state.value;
    const lookupTerm = value.toLowerCase().trim();
    let { lookupItems } = { ...this.state };
    if (lookupTerm.length == 4) {
      if (lookupTerm !== this.state.lookupTerm && !this.state.isLoading) {
        this.setState(state => ({ ...state, isLoading: true }));
        const params = {
          term: lookupTerm,
          categoryId: this.props.category ? this.props.category.id : ""
        };
        apiDev("search/lookup", params)
          .then(response => {
            const items = response.data;
            items.push(this.getEmptyListItem());
            this.setState(state => ({
              ...state,
              isLoading: false,
              lookupItems: response.data,
              lookupPayload: response.data,
              lookupTerm
            }));
          })
          .catch(e => {
            console.log(e);
          });
      } else {
        lookupItems = this.state.lookupPayload;
      }
    } else if (lookupTerm.length > 4) {
      const lookupPayload = [...this.state.lookupPayload];
      lookupItems = lookupPayload.filter((e, i) => {
        return e.title.toLowerCase().indexOf(lookupTerm) != -1;
      });
    } else {
      lookupItems = [];
    }

    this.setState(state => ({
      ...state,
      value,
      lookupItems
    }));
    this.props.valueHandler({});
  };

  clearHandler = () => {
    this.setState(state => ({
      ...state,
      value: "",
      lookupItems: []
    }));
    this.props.setTerm("");
    this.props.valueHandler({});
  };

  handleKeyDown = e => {
    if (e.key === "Enter") {
      this.setState(state => ({
        ...state,
        lookupItems: [],
        value: this.value
      }));
    }
  };
  // establishClas = () => {
  //   let;
  // };
  render = () => {
    // let currentItem = this.establishClas;
    const handler = this.onLookupItemClick;
    const items = this.state.lookupItems
      ? this.state.lookupItems.map((item, index) => (
          <li
            key={index}
            className={css["item"]}
            onClick={() => {
              handler(item);
            }}
          >
            {item.title}
            <span>{item.subtitle}</span>
            {item.categoryCode && item.partnerCode ? (
              <span className="ut-theme-color">
                <Link
                  to={
                    routes.CATALOG_PREFIX +
                    item.categoryCode +
                    "/" +
                    item.partnerCode
                  }
                >
                  Перейти
                </Link>
              </span>
            ) : null}
          </li>
        ))
      : null;

    const loader = this.state.isLoading ? (
      <img className={css["loading"]} src={loadingImg} />
    ) : (
      ""
    );

    return (
      <div className={css["group"]}>
        <input
          type="text"
          onChange={this.lookup}
          className={this.props.className}
          placeholder="Введите наименование..."
          value={this.value}
          onKeyDown={this.handleKeyDown}
        />
        {loader}
        {items.length > 0 ? (
          <div
            className={css["list-overlay"]}
            onClick={() => handler(this.getEmptyListItem())}
          ></div>
        ) : null}
        <ul className={css["list"]}>{items}</ul>
        {this.props.value.length > 0 ? (
          <a className={css["clear"]} onClick={this.clearHandler} />
        ) : (
          ""
        )}
      </div>
    );
  };
}

const mapStateToProps = state => {
  return {
    catalog: state.catalog,
    category: state.user.customerData.category,
    value: state.user.customerData.searchTerm,
    searchTerm: state.user.customerData.searchTerm
  };
};

const mapDispatchToProps = dispatch => ({
  setTerm: text => dispatch(actions.setTerm(text))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
  // withRouter
)(LookUpInput);
