import React from "react";
import css from "./Hero.module.scss";
import util from '../util/util';
import { Link } from 'react-router-dom';

const Offer = props => {
    const labels = [];/*props.labels.map((label,index)=>(
        <li key={index} className={css["offer-label"]}>
            {label.title}
        </li>
    ));*/
    const link = `/catalog/${props.parentCode}/${props.productId}`;
    return (
        <li
            className={css["offer"]}
            style={{backgroundImage:`url('${props.image}')`}}
        >
            <div className={`${css["offer-data"]} ut-theme-color`}>
                <p className={css["offer-title"]}>
                    {props.title}
                </p>
                <ul>
                    {labels}
                </ul>
                <div className={css["offer-price"]}>
                    <i className="ut-icon-sales1"></i> <span>{util.currencyFormat(props.price)} </span>
                </div>
                <div>
                    <button
                        onClick={props.cartHandler}
                        className={`${css["offer-btn"]} ${
                            css["offer-btn-icon"]
                        } ut-theme-color`}
                    >
                        <i className="ut-icon-cart1"></i>
                    </button>&nbsp;
                    <Link to={link} className={`${css["offer-btn"]} ut-theme-color`}>
                        <span>Бронировать</span>
                    </Link>
                </div>
            </div>
        </li>
    );
};

export default Offer;
