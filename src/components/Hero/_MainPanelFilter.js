import React, { useEffect, useState, useCallback } from "react";
import css from "./styles/Filter.module.scss";
import { Link } from "react-router-dom";

import Litepicker from "litepicker";
import "../../assets/scss/litepicker.scss";

import TimePicker from "../TimePicker/TimePicker";
import MorePicker from "../TimePicker/MorePicker";
import ChildPicker from "../TimePicker/ChildPicker";

import LookUpInput from "./_LookUpInput";
import { connect } from "react-redux";
import * as actions from "../../store/actions/index";
import util from "../util/util";
import * as routes from "../util/routes";

let picker;

const MainPanelFilter = props => {
  let fields = [];
  let ref = React.createRef();

  const getDateText = (singleMode = false) => {
    const dateIn = props.filter.dateIn.format("DD.MM.YYYY");
    if (singleMode) return dateIn;
    const dateOut = props.filter.dateOut
      ? props.filter.dateOut.format("DD.MM.YYYY")
      : "...";
    return `${dateIn} - ${dateOut}`;
  };

  const [chosenObject, chooseObject] = useState({});
  const [more, toggleMore] = useState(false);
  const filterConfig = useCallback(param => {
    let config = {};

    // If no category selected, or accommodation is selected -
    // double date picker,
    // else - single date

    config.singleMode = props.category ? !(props.category.id == 7) : false;
    config.datePickerClassName = config.singleMode
      ? util.join(css["input-text-inline"], css["input-text"])
      : css["input-text"];
    config.datePickerPlaceholder = getDateText(config.singleMode);
    if (param && config.hasOwnProperty(param)) {
      return config[param];
    }
    return config;
  });

  useEffect(() => {
    const config = filterConfig();
    const input = ref.current;
    if (picker) picker.destroy();

    picker = new Litepicker({
      element: input,
      singleMode: config.singleMode,
      numberOfMonths: 2,
      format: "DD.MM.YYYY",
      numberOfColumns: 2,
      minDate: util.moment().add(1, "day"),
      lang: "ru-RU",
      tooltipText: {
        one: "день",
        few: "дня",
        many: "дней"
      },
      onSelect: function(start, end) {
        const _end = end || util.moment(start).add(1, "day");

        props.setFilter({
          dateIn: start,
          dateOut: _end
        });
      }
    });
  }, [filterConfig, props, props.category, ref]);

  let searchLink = "/";
  if (props.searchTerm.length > 0) {
    const categoryParam = props.category ? `${props.category.code}/` : "";
    searchLink =
      routes.SEARCH_PREFIX +
      categoryParam +
      decodeURIComponent(props.searchTerm);
  }
  if (!util.emptyObject(chosenObject)) {
    if (chosenObject.categoryCode && chosenObject.partnerCode) {
      searchLink =
        routes.CATALOG_PREFIX +
        chosenObject.categoryCode +
        "/" +
        chosenObject.partnerCode;
    }
  }
  // let checkEnter = target => {
  //   if (target.charCode == 13) {
  //     return { searchLink };
  //   }
  //   return { searchLink };
  // };
  return (
    <div className={css["container"]}>
      <div className={css["group"]}>
        <div className={css["input-group"]}>
          <div className={css["input-group-inline"]}>
            <label className={`${css["input-label"]} ut-theme-color`}>
              Куда
            </label>

            <LookUpInput
              category={props.category}
              className={util.join(css["input-text-inlin"], css["input-text"])}
              valueHandler={chooseObject}
            />
          </div>
        </div>
      </div>

      <div className={css["group"]}>
        <div className={css["input-group"]}>
          <div className={css["input-group-inline"]}>
            <label className={`${css["input-label"]} ut-theme-color`}>
              Когда
            </label>
            <input
              type="text"
              ref={ref}
              className={filterConfig("datePickerClassName")}
              placeholder={filterConfig("datePickerPlaceholder")}
              readOnly={true}
              value={getDateText(filterConfig("singleMode"))}
            />
            {filterConfig("singleMode") ? (
              <TimePicker
                time={props.filter.time}
                pickHandler={(h, m) => props.setFilter({ time: h + ":" + m })}
                notTime={false}
              />
            ) : null}

            <label
              onClick={() => toggleMore(!more)}
              className={css["input-btn"]}
            >
              <i className="ut-icon-more"></i>
            </label>
          </div>

          <div className={util.join(css["more"], more && css["more--on"])}>
            <label className={`${css["input-label"]} ut-theme-color`}>
              С кем
            </label>
            <MorePicker
              adults={props.filter.adults}
              more={true}
              main={true}
              children={props.filter.children}
              pickHandler={(ad, ch) => {
                props.setFilter({
                  adults: parseInt(ad)
                });
                props.setFilter({
                  children: parseInt(ch)
                });
              }}
            />
            {fields}
          </div>
        </div>
        <Link
          className={util.join(
            css["btn"],
            props.searchTerm.length > 0 && "ut-theme-color",
            props.searchTerm.length > 0 && css["btn--on"]
          )}
          to={searchLink}
        >
          {" "}
          <span>найти</span> <i className="ut-icon-search1"></i>{" "}
        </Link>
      </div>
    </div>
  );
};

const mapStateToProps = state => ({
  category: state.user.customerData.category,
  filter: state.user.customerData.filter,
  searchTerm: state.user.customerData.searchTerm
});

const mapDispatchToProps = dispatch => ({
  setFilter: filter => dispatch(actions.setFilter(filter)),
  setTerm: filter => dispatch(actions.setTerm(filter))
});
export default connect(mapStateToProps, mapDispatchToProps)(MainPanelFilter);
