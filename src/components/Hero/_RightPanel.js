import React from "react";
import css from "./Hero.module.scss";
import Offer from "./_RightPanelOffer";
import util from '../util/util';

const RightPanel = props => {
    const offers = [];
    props.offers.forEach((offer, index) => {
        const itemData = {
            productId:offer.id,
            categoryId:offer.parentId,
            dateIn:util.moment().add(20,'day').format("YYYY-MM-DD"),
            dateOut:util.moment().add(27,'day').format("YYYY-MM-DD"),
            adults:1,
            children:0, 
            time:'10:00:00',
            Fname:'',
            Lname:'',
            email:'',
        };
        offers.push(
            <Offer
                parentCode={offer.parentCode}
                key={index}
                productId={offer.code}
                title={offer.name}
                labels={offer.labels}
                oldprice={offer.oldPrice}
                price={offer.price}
                image={offer.photos[0].small}
                cartHandler={()=>props.cartHandler(itemData)}
            />
        );
    });
    return (
        <div className={css["panel-right"]}>
            <div className={css['panel-bg']} style={{backgroundImage:`url(${props.backgroundImage})`}}/>
            <ul className={css["offers"]}>{offers.length>0? offers: ` `}   </ul>
        </div>
    );
};



export default RightPanel;
