import React, { useState, Fragment } from "react";
import classes from "./styles/LeftPanel.module.scss";
import Category from "./_LeftPanelCategory";
import * as actions from "../../store/actions/index";
import { connect } from "react-redux";
import util from "../util/util";
import LocationSelect from "./_LocationSelect";

const LeftPanel = props => {
    const setCategory = category => {
        if (category) {
            if (!props.category) {
                return props.setCategory(category);
            } else if (category.id != props.category.id) {
                return props.setCategory(category);
            } else {
                return props.setCategory(null);
            }
        }
        return props.setCategory(null);
    };
    const [locationModal, toggleLocation] = useState(false);

    let categoriesHtml = [];

    props.categories.forEach((category, index) => {
        const checked =
            Boolean(props.category) && props.category.id == category.id;
        categoriesHtml.push(
            <Category
                key={index}
                index={index}
                checked={checked}
                category={category}
                onClick={() => setCategory(category)}
            />
        );
    });

    return (
        <Fragment>
            {locationModal ? (
                <LocationSelect
                    pickHandler={location => {
                        props.setLocation(location);
                        toggleLocation(false);
                    }}
                    hideHandler={() => toggleLocation(false)}
                />
            ) : null}
            <div
                className={util.join(
                    classes["panel-left"],
                    Boolean(props.category) && classes["panel-left--solid"]
                )}
            >
                <p className={classes["heading"]}>Вы ищете...</p>
                <div className={classes["location"]}>
                    <i className="ut-icon-geo1 ut-theme-color"></i>
                    <a onClick={() => toggleLocation(true)}>
                        {props.location.country}, {props.location.city}
                    </a>
                </div>
                <form action="#search-category">
                    <ul className={classes["categories"]}>{categoriesHtml}</ul>
                </form>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    categories: state.catalog.categories,
    category: state.user.customerData.category,
    location: state.user.location
});

const mapDispatchToProps = dispatch => ({
    addToCart: item => dispatch(actions.addToCart(item)),
    setCategory: item => dispatch(actions.setCategory(item)),
    setLocation: location => dispatch(actions.setLocation(location))
});

export default connect(mapStateToProps, mapDispatchToProps)(LeftPanel);
