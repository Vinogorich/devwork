import util from '../components/util/util';
import img from '../assets/image/dummy_small.jpg';

const userObject = {
    firstName:'',
    lastName: '',
    gender:"f",
    image: img,
    email:"",
    phone:"",
    id:'',
    social: {
        vk: "",
        facebook: "",
        twitter:"",
        instagram:"",
        ok:"",
    }
};

export const createObject = (user)=> {
    const _user = {
        firstName:user.fname,
        lastName: user.lname,
        gender:user.sex && user.sex.substr(0,1).toLowerCase()==="m" ? "m" : "f",
        image: user.img && user.img.length > 5 ? user.img : null,
        email: user.email,
        phone: user.phone,
        id: user.id,
    }
    return util.mergeObjects(userObject,_user);
}
