import util from '../components/util/util';
import img from '../assets/image/dummy_small.jpg';

const responseObject = {
    partners:[],
    total:0,
    positions:0,
};

const partnerObject = {
    name:'',
    id:0,
    image:'',
    items:[],
    total:0,
    quantity:0
}

const itemObject = {
    name:'',
    id:0,
    text:'',
    params:'',
    total:0,
    quantity:0
}

const paramObject = {
    code:'',
    name:'',
    value:'',
    required:true,
    edit:false,
}

const createCart = (response) => {
    if ( !Array.isArray(response)) {
        return cartFactory.createResponse({ });

    }
        let total = 0;
        let numPositions = 0;
        const partners = {};
        response.map((element)=>{
            util.log('[E]',element);
            const partnerId = element.partner[0].id;
            if (!partners[partnerId]) {
                const _image = util.getPropertySafe(element,"partner.0.photo.0.small", img);
                const _partner = {
                    ...element.partner[0],
                    items:[],
                    image:_image
                }
                partners[partnerId] = cartFactory.createPartner(_partner);
                partners[partnerId].total = element.price * element.quantity;
                partners[partnerId].quantity = element.quantity;
            } else {
                partners[partnerId].total += element.price * element.quantity;
                partners[partnerId].quantity += element.quantity;
            }
            total +=element.price * element.quantity;
            numPositions ++;
            let params =[];
            if (Array.isArray(element.props)) {
                params = element.props.map((prop)=>{
                    const _prop = {
                        ...prop,
                        name:prop.title
                    };
                    return cartFactory.createParam(_prop);
                })
            }
            const _item = {
                ...element,
                params
            };
            const item = cartFactory.createItem(_item);
            util.log('[I]',item);
            partners[partnerId].items.push(item);
            
        })
        
        
        util.log('[P]',partners);
        return cartFactory.createResponse({
            partners: Object.values(partners),
            total,
            positions:numPositions
        });
}

 const cartFactory = {
    createResponse:(response)=>( util.mergeObjects(responseObject,response)),
    createPartner: (partner) =>( util.mergeObjects(partnerObject,partner)),
    createItem: (item) =>( util.mergeObjects(itemObject,item)),
    createParam: (param) => (util.mergeObjects(paramObject, param)),
}

export default createCart;

