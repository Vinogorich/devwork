export const createObject = (
    id, 
    name, 
    image, 
    categoryCode, 
    partnerCode, 
) => ({
    id, 
    name, 
    image, 
    categoryCode, 
    partnerCode, 
});