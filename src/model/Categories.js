class Categories {
    constructor(data) {
        this.data = data;
    }

    byId(id) {
        const _category = this.data.filter(item => item.id == id);
        return _category.length > 0 ? _category[0] : null;
    }
}

export default Categories;
